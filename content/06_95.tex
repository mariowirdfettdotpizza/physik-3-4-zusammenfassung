\subsection{Stationäre Störungsrechnung ohne Entartung}
Das Grundproblem lautet
\begin{align}
	\op{H}(\lambda) = \underbrace{\op{H_0}}_{leicht}+\lambda \underbrace{\op{V}}_{schwer},
\end{align}
\noindent dabei ist $\lambda$ ein Steuerparameter $\in [0,1]$. Das Eigenwertproblem zu $\op{H}_0$ sei bereits gelöst
\begin{align}
	\op{H}_0\ket{n} &= \epsilon_n \ket{n}\\
	\braket{n|n} &= 1\\
	\sum \ket{n}\bra{n} &= 1
\end{align}
\noindent gesucht sind also die Eigenzustände und -werte zu $\op{H}(\lambda)$:
\begin{align}
	\op{H}(\lambda)\ket{\Psi_n(\lambda)} = E_n(\lambda)\ket{\Psi_n(\lambda)}
\end{align}
\noindent Unter der Annahme, dass sich $\ket{\Psi_n}$ und $E_n(\lambda)$ stetig differenzierbar aus $\ket{n}$ und $\epsilon_n$ entwickeln lassen, setzt man eine Potenzreihe in $\lambda$ an (dies wäre bei Supraleitung z.B. nicht möglich).
\begin{align}
	\ket{\Psi_n} = \sum_{m}c_m(\lambda)\ket{m}
\end{align}
\noindent Die $c_m$ und $E_m$ werden nun nach $\lambda-$Potenzen entwickelt.
\begin{align}
	c_m(\lambda) &= c_m^{(0)}+\lambda c_m^{(1)}+\lambda^2 c_m^{(2)}+...\\
	E_m(\lambda) &= E_m^{(0)}+\lambda E_m^{(1)}+\lambda^2 E_m^{(2)}+...
\end{align}
\noindent In die Schrödingergleichung eingesetzt ergibt dies
\begin{align}
	(\op{H}_0+\lambda \op{V})\sum_{m}(c_m^{(0)}+\lambda c_m^{(1)}+\lambda^2 c_m^{(2)}+...)\ket{m}\\
	= (E_m^{(0)}+\lambda E_m^{(1)}+\lambda^2 E_m^{(2)}+...)\sum_{m}(c_m^{(0)}+\lambda c_m^{(1)}+\lambda^2 c_m^{(2)}+...)\ket{m}.
\end{align}
\noindent Da dies für alle $\lambda$ gelten muss, folgt ein Koeffizientenvergleich zwischen den Potenzen von $\lambda$. Dieser liefert in erster Ordnung das Ergebnis
\begin{align}
	E_{n}^{(1)} = \braket{n|\op{V}|n},
\end{align}
\noindent \textbf{die Energiekorrektur erster Ordnung entspricht also dem Erwartungswert des Störoperators in ungeteiltem Zustand}. Die Koeffizienten lassen sich mithilfe von
\begin{align}
	c_{k}^{(1)} &= \frac{\braket{k|\op{V}|n}}{E_{n}^{(0)}-E_{k}^{(0)}},\, k\neq n\\
	c_{n}^{(1)} &= 0
\end{align}
\noindent bestimmen. 

\noindent Es wird nun auch die 2. Ordnung betrachtet, der Koeffizientenvergleich geht also weiter und liefert die Energiekorrektur 2. Ordnung:
\begin{align}
	E_{n}^{(2)} = \sum_{m}c_{m}^{(1)}\braket{n|\op{V}|m} = \sum_{m \neq n}\frac{|\braket{n|\op{V}|m}|^2}{E_{n}^{(0)}-E_{m}^{(0)}}
\end{align}
\noindent \textbf{Die Energiekorrektur 2.Ordnung ist also die Summe des Betragsquadrates der Störungsmatrixelemente geteilt durch den Energienenner.}
\begin{enumerate}
\item Die Grundzustandsenergie in 2. Ordnung wird immer abgesenkt.
\item Wegen der Energienenner sind benachbarte $\epsilon$-Niveaus oft am wichtigsten.
\item Wenn $E_{m}^{(0)}$ nur wenig größer als $E_{n}^{(0)}$ ist, wird die Grundzustandsenergie $E_n$ nach unten gedrückt, man spricht von einander abstoßenden Energieniveaus.
\item Für die Zustände aus dem kontinuierlichen Spektrum müssen die Summen durch Intergrale ersetzt werden.
\item Die Störungsrechnung kann wenn überhaupt nur dann gut sein, wenn die Korrekturen zum ungestörten Zustand klein sind
\begin{align}
	c_k^{(0)} &= \left|\frac{\braket{k|\op{V}|n}}{E_{n}^{(0)}-E_{k}^{(0)}}\right| \ll 1.
\end{align}
\noindent Dadurch können jedoch Probleme mit (fast) entarteten Energieniveaus entstehen.
\end{enumerate}

\noindent Als Beispiel folgt der \textbf{Normale Zeemann-Effekt}. Betrachtet wird ein Wasserstoffatom im $\vec{B}$-Feld.
\begin{align}
	\op{H} = \op{H}_0-\frac{eB}{2m_e}\op{L_z}
	\label{eq:hmagn}
\end{align}
\noindent Die exakte Lösung lautet
\begin{align}
	E_{n,l,m} = E_{n}^0-\frac{eB}{2m_e}m\hbar.
\end{align}
Über die Energiekorrektur erster Ordnung
\begin{align}
	E_{n,l,m}^{(1)} = -\braket{nlm|\frac{eB}{2m_e}\op{L_z}|nlm} = -\frac{eB}{2m_e}m\hbar \underbrace{\braket{n,l,m|n,l,m}}_{=1}
\end{align}
\noindent erhält man hier ebenfalls das exakte Ergebnis. In 2. Ordnung ergibt sich
\begin{align}
	E_{n,l,m}^{(2)} = \braket{nlm|\op{L_z}|n'l'm'} = 0,
\end{align}
\noindent wenn nicht alle Quantenzahlen übereinstimmen.

\noindent Würde man nun mehrere Elektronen im Magnetfeld betrachten, wäre die Rechnung nicht mehr so einach, da die Elektronen miteinander wechselwirken. Der Gesamtdrehimpuls wäre neben dem gesamt-$\op{L_z}$ weiterhin eine Erhaltungsgröße. $\op{H}$ sähe zwar aus wie \eqref{eq:hmagn},  jedoch würde bereits $\op{H}_0$ kein exakt lösbares Problem mehr darstellen.

\noindent Weiterhin wird der \textbf{Stark-Effekt} betrachtet. Ein Wassersoffatom befindet sich in einem Elektrischen Feld in z-Richtung. Der Hamiltonoperator lautet
\begin{align}
	\op{H}=\op{H}_0-eF\op{z}.
\end{align}
Der Grundzustand hat die Form
\begin{align}
	\ket{nlm} &= \ket{100}\\
	\braket{\vec{r}|100} &= \Phi_{100}(r,\theta,\phi),
\end{align}
\noindent ist also kugelsymmetrisch. In 1. Ordnung ergibt die Störungsrechnung
\begin{align}
	E_{100}^{(1)} = -eF\braket{100|\op{z}|100} = -eF\int \d^3{r}|\Psi_{100}(r,\theta,\phi)|^2z = 0,
\end{align}
\noindent da der Integrand antisymmetrisch in $z$ ist. In 2. Ordnung erhält man
\begin{align}
	E_{100}^{(2)} = e^2F^2\sum_{(n,l,m)\neq((1,0,0)}\frac{|\braket{|nlm|\op{z}|100}|^2}{E_{1}^{(0)}-E_{n}^{(0)}},
	\label{eq:summe}
\end{align}
\noindent wobei die Summe die Kontinuumszustände mit einschließt. Erwartet wird nun, dass 
\begin{align}
	|\braket{|nlm|\op{z}|100}|^2 \propto a_0\\
	E_{1}^{(0)}-E_{n}^{(0)} \propto E_\text{Ry} = \frac{e^2}{8 \pi \epsilon_0 a_0}
\end{align}
\noindent gilt. Daraus folgt
\begin{align}
	|E_{100}^{(2)}| \propto e^2F^2\frac{a_{0}^2}{E_\text{Ry}} \ll E_\text{Ry} 
\end{align}, damit die Störungsrechnung sinnvoll ist. Die Größenordnung, die das Feld des Kerns auf das Elekton ausübt ist
\begin{align}
	|eF| \ll \frac{E_\text{Ry}}{a_{0}} = e\cdot 10^{11}\,\frac{\text{V}}{\text{m}}.
\end{align}
\noindent Um die Berechnung der Summe \eqref{eq:summe} zu umgehen, wird der Operator $\op{D}$ mit der Eigenscha
ft
\begin{align}
	\op{z}\ket{100} = (\op{D}\op{H}_0-\op{H}_0\op{D})\ket{100}
\end{align}
\noindent eingeführt, das Matrixelement aus \eqref{eq:summe} wird damit zu
\begin{align}
	(E_{1}^{(0)}-E_{n}^{(0)})\braket{nlm|\op{D}|100}
\end{align}
\noindent und die gesamte Summe nimmt die Form
\begin{align}
	\sum_{(n,l,m)\neq((1,0,0)}\frac{|\braket{|nlm|\op{z}|100}|^2}{E_{1}^{(0)}-E_{n}^{(0)}} = \sum_{(n,l,m)\neq((1,0,0)}\braket{100|\op{z}|nlm}\braket{nlm|\op{D}|100}
\end{align}
\noindent Über weitere Umformung und Ergänzung eines Terms um die Vollständigkeitsrelation auszunutzen vereinfacht sich die Summe weiter zu
\begin{align}
	\braket{100|\op{z}\op{D}|100}-\underbrace{\braket{100|\op{z}|100}\braket{100|\op{D}|100}}_{=0,\,\text{wegen}\,E_{100}^{(1)}}. 
\end{align}
\noindent In Ortsdarstellung findet man den Ausdruck
\begin{align}
	\op{D} = -\frac{ma_0}{\hbar^2}\left(\frac{\vec{r}}{2}+a_0\right)\op{z}
\end{align}
\noindent für $\op{D}$. Dieser Operator besitzt den Erwartungswert
\begin{align}
	E_{100}^{(2)} = -9\pi \epsilon_0 F^2 a_{0}^3.
\end{align}
Insgesamt ändert sich hier die Grundzustandsenergie des Wasserstostoffatoms also $\propto F^2$, was einem induzierten Dipolmoment $\propto F$ entspricht. Den Proportionalitätsfaktor nennt man die atomare Polarisierbarkeit (Dielektrizitätskonstante).

\noindent Betrachtet man angeregte Zustände, stellt man fest, dass die Zustände entartet sind und die Störungsrechnung modifiziert werden muss.