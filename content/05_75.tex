\subsection{Der harmonische Oszillator}
\subsubsection{Lösung der Schrödingergleichung des harmonischen Oszillators}
%150506 Skript
%150504 Tafeln
%150506 Tafeln
In der klassischen Mechanik ergibt sich der harmonische Oszillator durch eine lineare rücktreibende Kraft $F(x)=-Dx$ und einem daraus folgenden parabelförmigen Potential $V(x)=\frac{1}{2}Dx^2$. Daher betrachten wir bei dem quantenmechanischen Oszillator ebenfalls ein parabelförmiges Potential. Daher ergibt sich folgende Schrödinger-Gleichung:
\begin{equation}
\frac{d^2 \Phi(x)}{dx^2}+\frac{2m}{\hbar^2}\left( E-\frac{1}{2}Dx^2 \right) \Phi (x)=0 \label{DGLharmOszQM}
\end{equation}
Diese Differentialgleichung ist im Vergleich zu der des mechanischen harmonischen Oszillators schwer zu lösen, da die Koeffizienten nicht konstant sind. Folgende Eigenschaften der Lösung können vorhergesagt werden:
\begin{itemize}
\item Es gibt nur ein diskretes Energiespektrum, da das Potential für $x \rightarrow \pm \infty$ unbeschränkt ist. Die Bewegung des Teilchens im Raum ist also beschränkt
\item Zu jedem Energieeigenwert $E_n$ gehört genau eine Wellenfunktion $\Phi_n(x)$
\item $\Phi_n(x)$ hat genau n Nullstellen
\item Da $V(x)$ gerade ist, ist $\Phi_n(x)$ entweder eine gerade oder eine ungeade Funktion. Der Grundzustand $\Phi_0(x)$ ist eine gerade Funktion
\item Die Energieeigenwerte sind positiv. Falls $V(x)>V_{min}$ gilt, dann gilt ebenfalls $E_n>V_{min}$
\end{itemize}
Zur Vereinfachung der Differentialgleichung werden ein paar Ersetzungen durchgeführt:
\begin{equation}
\omega^2=\frac{D}{m} \qquad k=\sqrt{\frac{2mE}{\hbar^2}} \qquad \alpha =\sqrt[4]{\frac{mD}{\hbar^2}}=\sqrt{\frac{m\omega}{\hbar}}=\frac{1}{x_0}
\end{equation}
Dabei ist $x_0$ die charakteristische Länge. Es folgt die Weber'sche Differentialgleichung:
\begin{equation}
\frac{d^2 \Phi(x)}{dx^2} +(k^2 -\alpha^4 x^2) \Phi (x)=0
\end{equation}
Betrachtet man nun die Asymptotik, also $x \rightarrow \pm \infty$, ergibt sich auf Grund von $\alpha^2 x^2 \gg \frac{k^2}{\alpha^2}$:
\begin{equation}
\frac{d^2 \Phi_{\infty}(x)}{dx^2} -\alpha^4 x^2 \Phi_{\infty} (x)=0
\end{equation}
Die Lösung hierfür lautet:
\begin{equation}
\Phi_{\infty} (x)= e^{\pm \frac{1}{2}\alpha^2 x^2}
\end{equation}
Wobei der + Term auf Grund der Normierung ausscheidet. Bei genauerer Betrachtung stellt man zudem fest, dass diese Lösung die Differentialgleichung nicht exakt löst.\\
Um eine bessere allgemeinere Lösung zu erhalten, wird nun die Substitution $u=\alpha x$ durchgeführt und der Lösungsansatz $\Phi (u)= H(u) \exp{-\frac{1}{2}u^2}$ eingesetzt. Aus der Weber'schen Differentialgleichung wird nun:
\begin{equation}
\frac{d^2 H(u)}{du^2}-2u\frac{dH(u)}{du} +\left( \frac{k^2}{\alpha^2}-1\right) H(u)=0
\end{equation}
Diese Differentialgleichung wird nun mit einem Potenzreihenansatz gelöst.
\begin{equation}
H(u)=\sum_{m=0}^{\infty} a_m u^m
\end{equation}
Durch einsetzen und ein paar Umformungen ergibt sich:
\begin{equation}
\sum_{m=0}^{\infty} \underbrace{\left( (m+2)(m+1)a_{m+2} -2ma_m + \left( \frac{k^2}{\alpha^2}-1\right) a_m \right)}_{=0} u^m=0
\end{equation}
Nach dem Identitätssatz für analytische Funktionen muss die Klammer gleich null sein. Daraus ergibt sich eine rekursive Formel für die $a_m$:
\begin{equation}
a_{m+2} =\frac{2m-(\frac{k^2}{\alpha^2}-1)}{(m+2)(m+1)}a_m
\end{equation}
Für große m gilt somit $\frac{a_{m+2}}{a_m} \approx \frac{2}{m}$. Da dies ebenfalls für die Potenzreihe von $e^{u^2}$ gilt, würde $H(u)$ wie $e^{u^2}$ wachsen. Für große u würde somit für die Wellenfunktion $\Phi (u)$ gelten:
\begin{equation}
\Phi (u) \rightarrow e^{\frac{u^2}{2}}
\end{equation}
Da dies aber nicht mit der Normierbarkeit einer Wellenfunktion vereinbar ist, muss die Potenzreihe von H abbrechen. $H(u)$ ist daher ein Polynom. Aus der Abbruchbedingung $a_{m+2}=0$ für ein bestimmtes $n=m$ lässt sich folgern:
\begin{align}
2n+1=\frac{k^2}{\alpha^2}=\frac{2E}{\hbar \omega}\\
\Rightarrow E_n=(n+\frac{1}{2})\hbar \omega
\end{align}
Dies sind die Energieniveaus des harmonischen Oszillators. Die Wellenfunktion ist nun bestimmt als:
\begin{equation}
\Phi_n (x)= N_n H_n (\alpha x) e^{-\frac{1}{2} \alpha^2 x^2}
\end{equation}
Dabei ergeben sich die $H_n(u)$ aus der Rekursionsformel. Es handelt sich dabei um die Hermite-Polynome. Für die stationären Zustände ergibt sich die normierte Wellenfunktion:
\begin{equation}
\Phi_n (x)= \frac{1}{\sqrt{2^n n!\sqrt{\pi}x_0}}H_n(\frac{x}{x_0}) e^{-\frac{1}{2}\left(\frac{x}{x_0}\right)^2}
\end{equation}
\subsubsection{Algebraische Berechnung der Energieeigenwerte des harmonischen Oszillators}
Der Hamilton-Operator des harmonischen Oszillators lautet:
\begin{equation}
\hat{H}=\frac{\hat{p}^2}{2m}+\frac{1}{2}m\omega^2 \hat{x}^2, \qquad D=m\omega^2
\end{equation}
bzw. in Operatorenschreibweise:
\begin{equation}
\hat{H}\ket{\Phi_n}=E_n \ket{\Phi_n} \quad \text{bzw.} \quad \hat{H}\ket{n}=E_n \ket{n}
\end{equation}
Gesucht sind nun die Eigenvektoren $\ket{n}$ und die Eigenwerte $E_n$. Dafür werden zunächst die neuen, nicht hermiteschen Operatoren
\begin{equation}
\hat{a}=\frac{m\omega \hat{x} + \im \hat{p}}{\sqrt{2m\hbar \omega}} \quad \rightarrow \quad \hat{a}^{\dagger} =\frac{m\omega \hat{x} - \im \hat{p}}{\sqrt{2m\hbar \omega}}
\end{equation}
definiert. Für sie gilt 
\begin{equation}
\left[\op{a}, \opd{a} \right]=\op{1} \label{Kommutator_a}
\end{equation}

Weiterhin gelten die Beziehungen:
\begin{eqnarray}
\op{x}&=&(\op{a}+\opd{a})\sqrt{\frac{\hbar}{2m\omega}} \\
\op{p}&=&(\opd{a}-\op{a})\im\sqrt{\frac{m\omega\hbar}{2}}\\
\op{a}\ket{n}&=&\sqrt{n}\ket{n-1}\\
\opd{a}\ket{n}&=&\sqrt{n+1}\ket{n+1}
\end{eqnarray}



Der Hamilton-Operator des harmonischen Oszillators lässt sich somit auch beschreiben durch
\begin{equation}
\op{H}=\frac{ \hbar \omega }{2} \left( \op{a} \opd{a} + \opd{a} \op{a} \right)
\end{equation}
Mit (\ref{Kommutator_a}) folgt somit
\begin{equation}
\op{H}=\hbar \omega \left(\opd{a} \op{a} +\frac{1}{2} \op{1} \right) = \hbar \omega \left(\op{N} +\frac{1}{2} \op{1} \right) 
\end{equation}
Da der hermitesche Besetzungszahloperator $\op{N}$ die gleichen Eigenvektoren wie der Hamilton-Operator hat, betrachten wir zunächst:
\begin{equation}
\op{N} \ket{n}=n\ket{n}
\end{equation}
Vorab zwei nützliche Kommutatoren:
\begin{equation}
\left[ \op{N}, \op{a} \right] =-\op{a} \quad \text{und} \quad  \left[ \op{N}, \opd{a} \right] =\opd{a}
\end{equation}
Zur Herleitung der Eigenenergien mit der Dirac-Notation müssen drei Dingen gezeigt werden:
\begin{itemize}
\item Satz 1: Für die Eigenwerte des Operators $\op{N}$ gilt: $n\geq 0$\\
Wenn $\ket{n}$ ein Eigenvektor ist, dann gilt:
\begin{equation}
||\op{a} \ket{n}||^2=\braket{n|\opd{a} \op{a}|n}=\braket{n|\op{N}|n}=n \underbrace{\braket{n|n}}_{>0} \geq 0
\end{equation}
\item Satz 2: Sei $\ket{n}$ ein Eigenvektor von $\op{N}$ zum Eigenwert. Dann gilt:
\begin{enumerate}
\item Ist $n=0$, dann ist der Vektor $\op{a} \ket{0}=0$, da $||\op{a} \ket{n}||=0$ und nur die Norm des Nullvektors verschwindet.
\item Ist $n>0$, dann ist der Vektor $\op{a} \ket{n}$ ein nicht verschwindender Eigenvektor von $\op{N}$ zum Eigenwert n-1
\begin{align}
\left[ \op{N}, \op{a} \right] \ket{n} &= -\op{a} \ket{n}\\
\left[ \op{N}, \op{a} \right] \ket{n} &= \op{N} \op{a} \ket{n} - \op{a} \op{N} \ket{n} = \op{N} \op{a} \ket{n} - n \op{a} \ket{n}
\end{align}
\begin{equation}
\rightarrow \op{N} (\op{a} \ket{n}) =(n-1)\op{a} \ket{n}
\end{equation}
\end{enumerate}
\item Satz 3: Sei $\ket{n}$ ein Eigenvektor von $\op{N}$ zum Eigenwert n. Dann muss $n \in \mathbb{N}$ sein.\\
Angenommen es gäbe einen Eigenwert $m=n+\alpha$ mit $0<\alpha<1$
\begin{align}
\op{N} \ket{m} &=m \ket{m} =(n+\alpha) \ket{m} \\
\op{N} (\op{a}^n \ket{m})& =\alpha \ket{m} \neq 0 \qquad \text{Satz 1}\\
\op{N} (\op{a}^{n+1} \ket{m}) &=(\alpha -1) \ket{m}
\end{align}
Dies ist ein Widerspruch zu Satz 1, da $\op{a}^{n+1} \ket{m}$ ein Eigenvektor zum Eigenwert $\alpha-1<0$ sein müsste.
\end{itemize}
\begin{equation}
\rightarrow \op{N} \ket{n} =n \ket{n}  \qquad , n \in \mathbb{N}_0
\end{equation}
\begin{equation}
% 150506_tafeln.pdf S. 9
\op{H}=\hbar \omega (\op{N} +\frac{1}{2} \op{1}) \quad \rightarrow \quad \op{H} \ket{n} = \underbrace{ \hbar \omega(n+\frac{1}{2})}_{E_n} \ket{n} \quad
 \rightarrow \quad E_n = \hbar \omega(n+\frac{1}{2})
\end{equation}
\subsubsection{Berechnung der Wellenfunktion des Grundzustandes in der Ortsdarstellung}
Wegen $\op{a} \ket{0}=0$ (Satz 2.1) folgt, dass für die zum Eigenwert 0 gehörende Eigenfunktion $\Phi_0(x)=\braket{x|0}$ in der Ortsdarstellung gelten muss:
\begin{equation}
\frac{d\Phi_0(x)}{dx}+ \frac{x}{x_0^2}\Phi_0(x)=0 \qquad , x_0=\sqrt{\frac{\hbar}{m\omega}}
\end{equation}
Die normierte Lösung zu dem Grundzustand mit der Energie $E_0=\frac{1}{2}\hbar \omega$ lautet:
\begin{equation}
\Phi_0(x) =\frac{1}{\sqrt{\pi x_0}} e^{-\frac{1}{2}\left(\frac{x}{x_0}\right)^2} =\braket{x|0}
\end{equation}
Mit Hilfe des Operators $\opd{a}$ erhält man jeden angeregten Zustand aus dem Grundzustand durch
\begin{equation}
\ket{n} =\frac{1}{\sqrt{n!}}(\opd{a})^n \ket{0}
\end{equation}
Daher wird dieser Operator auch als Erzeugungsoperator bezeichnet. Dementsprechend kann man $\op{a}$ nutzen um auf einen niedrigeren Zustand zu gelangen.
\subsubsection{Unschärferelation}
\begin{align}
\text{Es ist: }\op{x}=\frac{x_0}{\sqrt{2}}(\op{a} + \opd{a}) \quad \rightarrow \quad &\braket{\op{x}} =\braket{n|\frac{x_0}{\sqrt{2}}(\op{a} + \opd{a})|n}=0\\
(\Delta x)^2 &= \braket{\op{x}^2}=\frac{x_0^2}{2}\braket{n|(\op{a} + \opd{a})^2|n}=x_0^2 \left( n+\frac{1}{2} \right)\\
\text{Es ist: }\op{p}=\frac{\hbar}{\sqrt{2}x_0 \im}(\op{a} - \opd{a}) \quad \rightarrow \quad &\braket{\op{p}} =\braket{n|\frac{\hbar}{\sqrt{2}x_0 \im}(\op{a} - \opd{a})|n}=0\\
(\Delta p)^2 &= \braket{\op{p}^2}=-\frac{\hbar^2}{2x_0^2}\braket{n|(\op{a} - \opd{a})^2|n}=\frac{\hbar^2}{x_0^2 } \left( n+\frac{1}{2} \right)
\end{align}
Daraus folgt das Unschärfeprodukt
\begin{equation}
\Delta x \cdot \Delta p= \hbar \left( n+ \frac{1}{2} \right) \geq \frac{\hbar}{2}
\end{equation}
\subsubsection{Vollständige Funktionensysteme}
Ein vollständiges Funktionensystem hat die folgenden Eigenschaften. (Dirac-Schreibweise)\\
\begin{equation}
\text{Orthogonalität: } (\Phi_n , \phi_m)= \intinfty \Phi_n(x) \cdot \Phi_m(x) dx =\delta_{nm} \qquad \left( \braket{n|m}=\delta_{nm} \right)
\end{equation}
\begin{equation}
\text{Vollständigkeit: } \sum_{n=0}^{\infty} \Phi_n(x) \cdot \Phi_n(x') =\delta (x-x') \qquad \left(\sum_{n=0}^{\infty} \ket{n} \bra{n} = \op{1} \right)
\end{equation}
\begin{equation}
\text{Entwicklung nach den }\Phi_n : f(x)=\sum_{n=0}^{\infty} c_n \Phi_n(x) \qquad \left( \ket{f} =\sum_{n=0}^{\infty} c_n \ket{n} \right)
\end{equation}
\begin{equation}
\text{Koeffizienten: } c_n = \intinfty f(x) \Phi_n(x) dx \qquad \left( \braket{f|n} \right)
\end{equation}
Für die zeitliche Entwicklung einer Wellenfunktion folgt zudem noch:
\begin{equation}
\Psi(x,t)=\sum_{n=0}^{\infty} c_n \Phi_n(x) e^{-\im \frac{E_n}{\hbar}t} \qquad \text{mit } c_n=\intinfty \Psi(x,0) \Phi_n(x) dx
\end{equation}