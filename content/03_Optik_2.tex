\subsection{Grundlagen der geometrischen Optik}
\subsubsection{Grundlagen}
Licht ist eine elektromagnetische Welle.
Eine ebene, monochromatische Welle kann so beschrieben werden:
\begin{align}
	E\left( \vec{r},t\right) &= E_0 \cos \left(\vec{k} \cdot \vec{r} - \omega t \right)
\end{align}

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{content/Bilder/Ebene_Welle.png}
		\caption{Eine Ebene Welle}
	\end{subfigure}		
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{content/Bilder/Kugelwelle.png}
		\caption{Eine Kugelwelle}
	\end{subfigure}		
	\caption{Der $\vec{k}$-Vektor zeigt die Ausbreitungsrichtung an}
\end{figure}
Der $\vec{k}$-Vektor steht senkrecht auf den Wellenfronten einer ebenen Welle.
Dadurch kann er die Welle vollständig repräsentieren und gibt so die \textbf{Richtung der Lichtstrahlen} an. \\
Bei Kugelwellen gilt dies ebenso:
\begin{align}
	E\left( \vec{r},t\right) &= \frac{E_0}{r} \cos\left( kr - \omega t \right)
\end{align}
\FloatBarrier
\subsubsection{Das Huygens'sche Prinzip}
Von jedem Punkt einer Wellenfront wird zur Zeit t eine Kugelwelle ausgesendet.
Die Überlagerung aller Elementarwellen zur Zeit $t + \Delta t$ ergibt die neue Wellenfront.
Wichtig ist dabei, dass die Frequenz aller Elementarwellen mit der, der Ursprungswelle übereinstimmt.
\begin{figure}
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{content/Bilder/Huygens_Ebene_Welle.png}
		\caption{Ebene Welle}
	\end{subfigure}		
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{content/Bilder/Huygens_Kugelwelle.png}
		\caption{Kugelwelle}
	\end{subfigure}
	\caption{Überlagerung der Elementarwellen ergeben neue Wellenfront}
\end{figure}
\FloatBarrier
\subsubsection{Maxwell- und Wellengleichungen in homogenen Dielektrika}
Die Maxwell'schen Gleichungen lauten:
\begin{align}
\vec{\nabla}\cdot\vec{D} &= \rho_\text{frei} & \vec{D} &= \varepsilon_\text{r}\varepsilon_0 \vec{E} \\
\vec{\nabla}\cdot\vec{B} &= 0 & \vec{B} &= \mu_\text{r}\mu_0\vec{H} \\
\vec{\nabla}\times\vec{E} &= - \frac{\partial\vec{B}}{\partial t} & & \\
\vec{\nabla}\times\vec{H} &= \vec{j}_\text{Trans.} + \frac{\partial\vec{D}}{\partial t}
\end{align}
Daraus ergibt sich:
\begin{align}
	\Delta \vec{E} - \frac{1}{c^2}\frac{\partial^2\vec{E}}{\partial t^2} &= 0 &
	\text{mit}\qquad c &= \frac{1}{\sqrt{\mu_0\mu_\text{r}\varepsilon_0\varepsilon_\text{r}}}
\end{align}
Die Lichtgeschwindigkeit in einem Medium beträgt also nur $c_0 / \sqrt{\mur\varepsilonr}$.
Aus dem Quotient der Lichtgeschwindigkeiten im Vakuum und im Medium ergibt sich der Brechungsindex:
\begin{align}
	n = \frac{c_0}{c} = \sqrt{\mu_0\mu_\text{r}\varepsilon_0\varepsilon_\text{r}}
	\approx \sqrt{\varepsilonr}, \quad\text{wegen}~~\mur \approx 1
\end{align}
\subsection{Brechung und Reflexion}
\subsubsection{Brechungsindex und Polarisation}
Wasser hat ein $\varepsilonr \approx 80$, $n_\text{Wasser}$ müsste also $\approx 9$ sein, 
der Brechungsindex liegt in der Realität jedoch nur bei \num{1,33}.
Das liegt an der Polarisierbarkeit $\alpha$ des Mediums. Es gilt folgendes:
\begin{align}
	\varepsilonr = 1 + \frac{\alpha}{\epsilon_0} = 1 + \chi_\text{e} = n^2
\end{align}
$\chi_\text{e}$ ist die \textit{Suszeptibilität} des Mediums. \\
Über ein ein einfaches Dispersionsmodell, in dem die Elektronen in dem Medium durch
einen harmonischen gedämpften Oszillator angenommen werden, erhält man folgende
Dielektrizitätsfunktion:
\begin{align}
	\varepsilonr (\omega) = \varepsilonr'(\omega) + \im\varepsilonr''(\omega) = n^2(\omega)
\end{align}
Dabei ist $\varepsilon''(\omega)$ die Energiedissipation.
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{content/Bilder/Dielektrizitaetsfunktion.png}
	\caption{Dielektrizitätsfunktion für ein einfaches harmonisches Modell}
\end{figure}
\FloatBarrier
Für den Frequenzabhängigen Brechungsindex von Wasser siehe Skript \textit{150107.pdf}, Seiten 15 ff. (bzw. 919 ff.)

\subsubsection{Reflexions und Brechungsgesetz}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{content/Bilder/Spiegel.png}
	\caption{Beim Spiegel wird immer vorne und hinten vertauscht (nicht links und rechts)}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{content/Bilder/Reflexion_Wasser_Luft.png}
	\caption{Brechung und (Total-)Reflexion am Übergang Wasser - Luft}
	\label{kalla_Reflexion_Wasser_Luft}
\end{figure}
Bei der Brechung an einer Grenzfläche ist der Winkel zwischen dem Lichtstrahl und der Fläche, die
senkrecht zu der Grenzfläche steht, immer im optisch dünneren Medium größer.
(siehe Abb. \ref{kalla_Verhalten_an_Grenzflaeche}) \\
Totalreflexion kann nur beim Übergang von einem optisch dichteren in ein optisch dünneres Medium
stattfinden (z.B. von Wasser in Luft, siehe Abb. \ref{kalla_Reflexion_Wasser_Luft}).
\FloatBarrier
\begin{figure}
	\begin{minipage}{0.45\textwidth}
		\centering
		\includegraphics[width=\textwidth]{content/Bilder/Verhalten_an_Grenzflaeche.png}
		\caption{\\Verhalten an einer ebenen Grenzfläche}
		\label{kalla_Verhalten_an_Grenzflaeche}
	\end{minipage}
	\hfill
	\begin{minipage}{0.45\textwidth}
		\subsubsection*{Reflexionsgesetz}
		Einfallswinkel gleich Ausfallswinkel:
		\begin{align}
			\alpha_1 &= \alpha_2
		\end{align}
		Außerdem liegt der reflektierte Strahl in der Einfallsebene.
		\subsubsection*{Snellius-Brechungsgesetz}
		\begin{align}
			\frac{\sin \alpha_1}{\sin \alpha_2} &= \frac{n_2}{n_1}
		\end{align}
		Im Spezialfall, dass Medium 1 das Vakuum (bzw. Luft) mit $n_1 = 1$, folgt:
		\begin{align}
			\frac{\sin \alpha_1}{\sin \alpha_2} &= n_2
		\end{align}
	\end{minipage}
\end{figure}
\FloatBarrier
\subsubsection{Lichtbrechung in einem Prisma}
\begin{figure}
	\begin{minipage}{0.45\textwidth}
		\includegraphics[width=\textwidth]{content/Bilder/Prisma.png}
		\caption{\\Strahlengang in einem Prisma}
	\end{minipage}
	\hfill
	\begin{minipage}{0.45\textwidth}
		Wenn die Umgebung Luft/Vakuum ist, und das Prismenmaterial den Brechungsindex $n_\text{P}$,
		dann ergibt sich für den Zusammenhang des Ablenkwinkels $\Theta$ mit dem Prismenwinkel
		$\gamma$:
		\begin{align}
			\sin\left(\frac{\Theta+\gamma}{2}\right) &= n_\text{P} \sin(\frac{\gamma}{2})
		\end{align}
	
	\end{minipage}
\end{figure}
\FloatBarrier

\subsubsection{Das Fermat'sche Prinzip}
\begin{figure}
	\begin{minipage}{0.3\textwidth}
		\includegraphics[width=\textwidth]{content/Bilder/Fermatsches_Prinzip.png}
	\end{minipage}
	\hfill
	\begin{minipage}{0.6\textwidth}
		$\Rightarrow$ Das Licht wählt auf dem Weg von einem Punkt $P_1$ zu einem Punkt $P_2$ immer den
		\textit{zeitlich kürzesten} - bzw. allgemeiner einen \textit{extremalen} - Weg. \\ \\
		Wegen $n\cdot c = \textit{const}$ gilt:
	\end{minipage}
\end{figure}
\FloatBarrier
\vspace{-20pt}
\begin{align}
	\textit{const}\cdot(t_2 - t_1)
	= \int\limits^{t_2}_{t_1}\!nc\,\mathrm{d}t
	= \int\limits^{t_2}_{t_1}\!n\frac{\mathrm{d}s}{\mathrm{d}t}\,\mathrm{d}t
	= \int\limits^{P_2}_{P_1}\!n(\vec{r})\,\mathrm{d}s
\end{align}
Dann lässt sich das Fermat-Prinzip als Variationsprinzip formulieren:
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{content/Bilder/Fermat-Variation.png}
\end{figure}
\FloatBarrier
\subsubsection{Beispiele}
Für einige Beispiele zum Fermat'schen Prinzip (Herleitung des Brechungs- und Reflexionsgesetzes,
Kontinuierlicher Brechungsindex, Fata Morgana) \\
\begin{center}
$\Rightarrow$ siehe Skript \textit{150107.pdf}, Seite 39 ff. (bzw. 942 ff.)
\end{center}

\subsubsection{Wellenlängenabhängiger Brechungsindex}
Der Brechungsindex hängt von der Wellenlänge des Lichts ab: $n = n(\lambda)$ \\
Bei \textit{normaler Dispersion} nimmt $n$ mit zunehmender Wellenlänge $\lambda$ ab,
der Ablenkwinkel $\Theta$ des Lichtstrahls nimmt ebenfalls ab. $\Rightarrow$ \textcolor{blue}{Blaues} Licht
wird stärker abgelenkt als \textcolor{red}{Rotes}.
\begin{figure}
	\centering
	\includegraphics[scale = 0.6]{content/Bilder/Frequenzabhaengiger_Brechungsindex.png}
\end{figure}

\subsubsection{Beispiel: Der Regenbogen}
\begin{center}
$\Rightarrow$ siehe dazu Skript \textit{150109.pdf}, Seite 1 ff. (bzw. 955 ff.)
\end{center}

\subsubsection{Wiederholung: Verhalten von E- und B-Feld an Grenzflächen}
An Grenzflächen gilt:
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{content/Bilder/Sprungverhalten_an_Grenzflaeche.png}
	\caption{Die senkrechte Komponente des E-Feldes und die parallele Komponente des B-Feldes springen an der Grenzfläche}
\end{figure}
\FloatBarrier

\subsubsection{Intensitäten bei Reflexion und Brechung}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{content/Bilder/Reflexion_Brechung_Intensitaeten.png}
\end{figure}
\textbf{s-Polarisation}: E-Feld senkrecht zur Einfallsebene \\
\textbf{p-Polarisation}: E-Feld parallel zur Einfallsebene \\ \\
Definition des Reflexions- und Transmissionskoeffizienten:
Die Reflexions- und Transmissionskoeffizienten geben den Anteil der einfallenden Amplitude an,
der reflektiert, bzw. transmittiert (also gebrochen) wird.
\begin{align}
	&\text{s-Polarisation} & &\text{p-Polarisation} \\
	&r_\text{s}(\alpha) = \frac{A_\text{rs}}{A_\text{es}} &
	&r_\text{p}(\alpha) = \frac{A_\text{rp}}{A_\text{ep}} \\
	&t_\text{s}(\alpha) = \frac{A_\text{gs}}{A_\text{es}} &
	&t_\text{p}(\alpha) = \frac{A_\text{gp}}{A_\text{ep}}
\end{align}
Der Reflexions- bzw. Transmissionsgrad gibt den Anteil der einfallenden Leistung an, die
reflektiert, bzw. transmittiert (also gebrochen) wird.
\begin{align}
	R_\text{s}(\alpha) &= {\left\vert\frac{A_\text{rs}}{A_\text{es}}\right\vert}^2 &
	R_\text{p}(\alpha) &= {\left\vert\frac{A_\text{rp}}{A_\text{ep}}\right\vert}^2 \\
	T_\text{s}(\alpha) &= {\left\vert\frac{A_\text{gs}}{A_\text{es}}\right\vert}^2 \frac{n_2 \cos \beta}{n_1 \cos \alpha}
	= {\left\vert\frac{A_\text{gs}}{A_\text{es}}\right\vert}^2 \frac{\tan \alpha}{\tan \beta} &
	T_\text{p}(\alpha) &= {\left\vert\frac{A_\text{gp}}{A_\text{ep}}\right\vert}^2 \frac{n_2 \cos \beta}{n_1 \cos \alpha}
	= {\left\vert\frac{A_\text{gp}}{A_\text{ep}}\right\vert}^2 \frac{\tan \alpha}{\tan \beta}
\end{align}

\subsubsection{Die Fresnel-Formeln}
\textbf{s-Polarisation}
\begin{align}
	r_\text{s}(\alpha) = \frac{n_1 \cos \alpha - n_2 \sqrt{1 - \left({n_1}^2 / {n_2}^2\right) \sin^2 \alpha}}
	{n_1 \cos \alpha + n_2 \sqrt{1 - \left({n_1\!}^2 / {n_2\!}^2\right) \sin^2 \alpha}} \\
	t_\text{s}(\alpha) = \frac{2 n_1 \cos \alpha}
	{n_1 \cos \alpha + n_2 \sqrt{1 - \left({n_1\!}^2 / {n_2\!}^2\right) \sin^2 \alpha}}
\end{align}
\textbf{p-Polarisation}
\begin{align}
	r_\text{p}(\alpha) = \frac{n_2 \cos \alpha - n_1 \sqrt{1 - \left({n_1}^2 / {n_2}^2\right) \sin^2 \alpha}}
	{n_2 \cos \alpha + n_1 \sqrt{1 - \left({n_1\!}^2 / {n_2\!}^2\right) \sin^2 \alpha}} \\
	t_\text{p}(\alpha) = \frac{2 n_1 \cos \alpha}
	{n_2 \cos \alpha + n_1 \sqrt{1 - \left({n_1\!}^2 / {n_2\!}^2\right) \sin^2 \alpha}}
\end{align}
Für Beispiele und Diagramme siehe Skript \textit{150109-Teil2.pdf}, Seiten 15 ff. (bzw. 1008 ff.)

\subsubsection{Der Brewster-Winkel}
Beim Brewster-Winkel $\alpha_\text{B}$ ist der Reflexionskoeffizient im Fall der p-Polarisation $r_\text{p} = 0$.
Durch Umformungen erhält man dann:
\begin{align}
	\tan \alpha_\text{B} = \frac{n_2}{n_1}
\end{align}
Außerdem ist der Brewster-Winkel der Punkt, an dem der reflektierte und der transmittierte Stahl
senkrecht aufeinander stehen ($\alpha_\text{B} + \beta_\text{B} = \SI{90}{\degree}$). \\ \\
Für weitere Beispiele und Diagramme siehe Skript \textit{150112.pdf}, Seiten 8 ff. (bzw. 1014 ff.)

\subsubsection{Totalreflexion}
Die Totalreflexion entsteht an einem Übergang von einem optisch dichteren in ein optisch dünneres Medium ($n_2 > n_1$).
Dabei wird der Strahl so gebrochen, dass er das Medium nicht mehr verlassen kann ($\beta > \frac{\pi}{2}$):
\begin{align}
	n_2 \sin \alpha_\text{total} &= n_1 \sin\left(\frac{\pi}{2}\right) = n_1 \\
	\Rightarrow \sin \alpha_\text{total} &= \frac{n_1}{n_2}, \qquad\qquad\text{bei }n_2 > n_1
\end{align}

\subsubsection{Evaneszente Wellen}
Eine total reflektierte Welle dringt - exponentiell abnehmend - doch in das dünnere Medium ein.
Für $n_1 > n_2$ und $\sin \alpha > \sin \alpha_\text{T} = n_2 / n_1$ gilt:
\begin{align}
	&k_\text{gy} = -\im k\sqrt{{n_1\!}^2 \sin^2 \alpha - {n_2\!}^2} \\
	&\Rightarrow k_\text{gy} = - \im \kappa,\quad\kappa = k\sqrt{{n_1\!}^2 \sin^2 \alpha - {n_2\!}^2} \\
	&\vec{E}_\text{g} = \vec{A}_\text{g} e^{\im (k_\text{gx}x - \omega t)} e^{-\frac{\vert y \vert}{\Lambda}},\qquad \Lambda = \frac{1}{\kappa}
\end{align}
Die Amplitude dieser Welle nimmt mit der Eindringtiefe $\Lambda$ exponentiell ab und breitet sich
parallel zur Grenzfläche aus.
\begin{align}
	\text{Eindringtiefe:\quad} \Lambda = \frac{\lambda}{2\pi\sqrt{{n_1\!}^2 \sin^2 \alpha - {n_2\!}^2}} \propto \lambda
\end{align}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{content/Bilder/Evaneszente_Welle.png}
	\caption{Evaneszente Wellen erscheinen hinter der Barriere}
\end{figure}
\FloatBarrier

\subsubsection{Absorption}
\begin{align}
	\varepsilonr &= \varepsilonr' + \im \varepsilonr'' & \varepsilonr'' &\rightarrow \text{Leitfähigkeit}~\sigma \\
	\Rightarrow n &= \sqrt{\varepsilonr} = n' + \im n'' &
	n' &\rightarrow \text{Brechung \& Reflexion},\quad n'' \rightarrow \text{Absorption}
\end{align}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{content/Bilder/Absorption.png}
\end{figure}
\FloatBarrier
Das Lambert-Beer-Gesetz
\begin{align}
	e^{\im(nkx-\omega t)} = e^{\im((n'+\im n'')kx - \omega t)} 
	= e^{-n''kx} e^{i(kn'x-\omega t)} \\
	e^{-n''kx} = e^{-x / l_0},\qquad l_0: \text{Absorpstionslänge}
\end{align}

\subsection{Optische Elemente}
\subsubsection{Einfache optische Elemente}
Für das Prinzip der optischen Abbildung und einigen Beispielen hierzu (Lochkamera)\\
siehe Skript \textit{150112-Teil2.pdf}, Seite 2 ff. (bzw. 1037 ff.)

\subsubsection{Linse (konvex)}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{content/Bilder/Linse_konvex.png}
\end{figure}
Ein achsenparalleler Strahl fällt auf die Kugeloberfläche und wird um den Winkel
$\alpha_3$ zur Achse hin abgelenkt. Beim Austritt brincht der Strahl erneut an der
Grenzfläche zur Achse hin, sodass er im Abstand $f$ mit dem Winkel $\alpha_4$
auf die optische Achse trifft. Für achsenparallele Strahlen gilt die \textbf{Linsenschleiferformel}:
\begin{align}
	\frac{1}{f} = (n-1)\left(\frac{1}{r_1} + \frac{1}{r_2}\right)
\end{align}
Für diese Formel wird eine \textbf{dünne Linse} vorausgesetzt. Außerdem
gilt die Beziehung nur für achsennahe Strahlen. Für eine symmetrische Linse
mit $r_1 = r_2 = r$ folgt sofort:
\begin{align}
	f = \frac{r}{2(n-1)}
\end{align}
Der Punkt $F$, in dem sich parallele Strahlen treffen heißt \textbf{Brennpunkt},
der Abstand $f$ ist die \textbf{Brennweite}. \\ \\
Für weitere Beispiele für fokussierende oder streuende Linsen und Spiegel\\
$\Rightarrow$ siehe Skript \textit{150115-Teil2.pdf}, Seiten 8 ff. (bzw. 1043 ff.)

\subsubsection{Abbildungsgesetz für dünne Linsen}
\begin{figure}
	\centering
	\begin{minipage}{0.4\textwidth}
		\includegraphics[width=\textwidth]{content/Bilder/Brennebene.png}
	\end{minipage}
	\quad
	\begin{minipage}{0.4\textwidth}
		Parallele Strahlen werden hinter einer konvexen dünnen Linse in der \textbf{Brennebene} fokussiert.
		Für achsennah einfallende Strahlen gilt also:
		\begin{align}
			\tan \alpha = \frac{x}{f}
		\end{align}
	\end{minipage}
\end{figure}
\FloatBarrier
\subsubsection{Die Linsengleichung}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{content/Bilder/Strahlengang.png}
	\caption{Veranschaulichung des Strahlenganges bei einer Abbildung per Linse}
	\label{kalla_Strahlengang}
\end{figure}
Aus Abb. \ref{kalla_Strahlengang} erhält man sofort die wichtige \textbf{Linsengleichung}:
\begin{align}
	\frac{1}{f} = \frac{1}{g} + \frac{1}{b}
\end{align}
Die \textbf{Brechkraft} einer Linse ist durch den Kehrwert der Brennweite definiert:
\begin{align}
	D = \frac{1}{f} \quad [D] = \frac{1}{m} = \SI{1}{\text{Dioptrie}}
\end{align}
Für einen Versuch und Beispiele hierzu\\
$\Rightarrow$ siehe Skript \textit{150115-Teil2.pdf},Seiten 25 ff. (bzw. 1060 ff.)

\subsubsection{Die sphärische Abberation}
Strahlen, die achsennah einfallen, werden im Brennpunkt $f_\text{i}$ fokussiert.
In genauerer Näherung gilt aber für die Brennweite ($d$ ist Abstand von der Achse):
\begin{align} \label{kalla_sphaerische_Abberation}
	f = R\left(\frac{n}{n-1} - \frac{1}{2n(n-1)}\frac{d^2}{R^2} + O\left(d^4 / R^4\right) \right)
\end{align}
Für Strahlen, bei denen nicht mehr $d \ll R$ gilt, ist die Brennweite also kürzer,
wegen dem 2. Term in (\ref{kalla_sphaerische_Abberation}).
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{content/Bilder/sphaerische_Abberation.png}
	\caption{Randstrahlen werden stärker abgelenkt}
\end{figure}
\FloatBarrier
\subsubsection{Die chromatische Abberation}
\begin{figure}
	\begin{minipage}{0.45\textwidth}
		\includegraphics[width=\textwidth]{content/Bilder/chromatische_Abberation.png}
	\end{minipage}
	\hfill
	\begin{minipage}{0.5\textwidth}
		Die chromatische Abberation tritt ebenfalls hauptsächlich bei Lichtstrahlen auf, die nicht
		achsennah sind. Blaues Licht wird hier stärker gebrochen als Rotes, dadurch ist
		\begin{align}
			f_\text{Blau} < f_\text{Rot}
		\end{align}
	\end{minipage}
\end{figure}
\FloatBarrier
\subsubsection{Beispiele}
Für Beispiele zu verschiedenen Themen zu Abbildungsfehlern, Korrekturen, Brillen, dem Auge und
optischen Täuschungen\\
$\Rightarrow$ siehe Skript \textit{150116.pdf}, Seiten 10 ff. (bzw. 1111 ff.)
\FloatBarrier


\subsection{Matrizenoptik}
\subsubsection{Grundlegende Prinzipien}
Bedingungen für die Matrizenoptik
\begin{itemize}
	\item $\lambda \ll$ alle Abmessungen ($\rightarrow$ geometrische Optik)
	\item axialsymmetrische Systeme, nur meridiane Strahlen
	\item achsennahe Strahlen in kleinen Winkeln ($\sin \alpha \approx \tan \alpha \approx \alpha$)
	zur Achse
	\item Brechungsindex $n$ (stückweise) konstant $\Rightarrow$ (stückweise) gerade Strahlen.
\end{itemize}
\begin{figure}
	\begin{minipage}{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{content/Bilder/Strahlparameter.png}
	\end{minipage}
	\hfill
	\begin{minipage}{0.6\textwidth}
		Ein Lichtstrahl wird auf einer Referenzebene (RE) (Ebene $\perp$ Symmetrieachse)
		durch zwei Parameter beschrieben:
		\begin{itemize}
			\item Achsenabstand $y$
			\item Steigung $v$
		\end{itemize}
	\end{minipage}
\end{figure}
Ein beliebig kompliziertes zusammengesetztes System aus optischen Elementen,
die sich als lineare Operationen beschreiben lassen, kann man als Produkt einiger
$2\times2$ Matrizen schreiben.
\FloatBarrier
\subsubsection{Translationsmatrix}
\begin{figure}
	\begin{minipage}{0.3\textwidth}
		\centering
		\includegraphics[width=\textwidth]{content/Bilder/Fortpflanzung_im_homogenen_Medium.png}
	\end{minipage}
	\hfill
	\begin{minipage}{0.6\textwidth}
		Mit
		\begin{align}
			V := nv \quad T := \frac{t}{n} \quad Y := y
		\end{align}
		gilt für die Fortpflanzung von Strahlen im homogenen Medium:
		\begin{align}
			\vecII{Y_2}{V_2} = \matII{1}{T}{0}{1} \vecII{Y_1}{V_1}
		\end{align}
	\end{minipage}
\end{figure}
\FloatBarrier
$\matII{1}{T}{0}{1}$ ist die Translationsmatrix für ein homogenes Medium (Länge $t$,
Brechungsindex $n$).
\subsubsection{Brechung an Kugelfläche}
Für schwach gekrümmte Kugelflächen gilt folgende \textbf{Refraktionsmatrix}:
\begin{align}
	R = \matII{1}{0}{-P}{1}\quad\text{mit}~\textbf{Brechkraft}~P=\frac{n_2-n_1}{r}
\end{align}
$P > 0$, wenn $n_2 > n_1$, Krümmungsmittelpunkt rechts, Strahl kommt von links $\rightarrow$ Sammel-Effekt.
Für den Grenzfall $r \rightarrow \infty$ ist $R = $\textit{Einheitsmatrix}.\\
Durch Multiplikation der Abbildungsmatrizen ergibt sich das Brechungsgesetz.
\subsubsection{Die dünne bikonvexe Linse}
Bei symmetrischen dünnen Linsen ergibt sich allgemein:
\begin{align}
	M_\text{L} = \matII{1}{0}{-\frac{1}{f}}{1}
\end{align}
\subsubsection{Abbildung durch eine Linse}
\begin{figure}
	\begin{minipage}{0.3\textwidth}
		\includegraphics[width=\textwidth]{content/Bilder/Abbildung_durch_Linse.png}
	\end{minipage}
	\hfill
	\begin{minipage}{0.6\textwidth}
		Ein Strahl im Abstand $a$ (\textit{Gegenstandsweite}) vor der Linse wird im Abstand $b$
		(\textit{Bildweite}) hinter der Linse so abgebildet:
		\begin{align}
			\vecII{Y_2}{V_2} &= \matII{1}{b}{0}{1}\matII{1}{0}{-\frac{1}{f}}{1}
			\matII{1}{a}{0}{1} \vecII{Y_1}{V_1} \\
			&= \matII{1-\frac{b}{f}}{a+b-\frac{ab}{f}}{-\frac{1}{f}}{1-\frac{a}{f}}\vecII{Y_1}{V_1}
		\end{align}
	\end{minipage}
\end{figure}
\FloatBarrier
$a_\text{nm}$ sei Komponente der Matrix $\matII{a_{11}}{a_{12}}{a_{21}}{a_{22}}$
\begin{itemize}
	\item $a_{11} = 0$: Ein unendlich weiter Gegenstand wird in die Brennebene rechts von der Linse
	abgebildet ($a = \infty$).
	\item $a_{22} = 0$: Ein Gegenstand in der Brennebene links von der Linse wird ins Unendliche
	abgebildet ($b = \infty$).
	\item $a_{12} = 0$: $\frac{1}{a} + \frac{1}{b} = \frac{1}{f}$ (Linsenformel) Alle vom Gegenstand
	in $Y_1$ ausgehenden Strahlen werden in $Y_2$ im Bild vereinigt. \textbf{Es findet eine Abbildung
	statt}, das Bild steht Kopf. Die Vergrößerung ist dabei:
	\begin{align}
		M = \frac{Y_2}{Y_1} = -\frac{b}{a} < 0
	\end{align}
	\item $a_{21} = 0$: Ist mit einer Linse nicht zu machen.
\end{itemize}
\subsubsection{Beispiele für optische Geräte}
Für Beispiele von optischen Geräten (Fernrohr, Teleskop, Mikroskop, etc.)\\
siehe Skript \textit{150114.pdf}, §48, Seiten 11 ff. (bzw. 1084 ff.)


\newpage