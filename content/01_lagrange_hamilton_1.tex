\subsection{Grundlagen}

Lagrange-Hamilton-Mechanik als...
\begin{enumerate}
\item große \glqq Vereinfachung\grqq{} zur Bestimmung der Bewegungsgleichungen eines mechanischen Systems
\item Erklärung eines größeren theoretischen, noch grundlegenderen Konzept in der Physik
\item Übergang zur Quantenmechanik
\end{enumerate}

\subsubsection{Einleitung: \glqq Umständlichkeit\grqq{} der Newton Mechanik}

\textbf{Beispiele für \glqq komplizierte\grqq{} Systeme:}
\begin{enumerate}
\item Tippy-Top Kreisel 
\item Das gekoppelte Doppelpendel 
\item Die Doppelte Atwood'sche Fallmaschine
\item Verschiedene Körper rollen eine schiefe Ebene hinab
\end{enumerate}

\textbf{Allgemeines Problem:}

Alle Kraftvektoren $\vec{F_j}$ der N-Massen $m_j$ mit Ortsvektoren $ \vec{r_j}$ müssen bestimmt werden.
\begin{equation*}
m_j \frac{d^2 \vec{r_j}(t)}{dt^2} = \sum_n \vec{F_1}\left( \vec{r_j}(t),..., \vec{r_N}(t), \dot{\vec{r_1}}(t),...,\dot{\vec{r_1}}(t), t\right) = \vec{F_j}
\end{equation*}
$\Rightarrow$ Lösung besteht aus 3N DGL's

\vspace{\baselineskip}

\textbf{Trägheitsmomente:}
\begin{eqnarray*}
\text{Kugel}&:& \qquad I_S = \frac{2}{5}mR^2\\
\text{Zylinder} &:& \qquad I_S = \frac{1}{2}mR^2\\
\text{Dünner Hohlzylinder} &:& \qquad I_S = mR^2
\end{eqnarray*}


\subsubsection{Das d'Alembert'sche Prinzip}

\textbf{Zwangskräfte:}\\
Wird ein System aus $N$ Massepunkten an den Orten $\vec{r_1},...,\vec{r_N}$ welche $s$ Zwangsbedingungen unterliegen betrachtet, so gilt:

\begin{enumerate}
\item Holonome Zwangsbedingungen:
Die Zwangsbedingungen können durch Gleichungen beschrieben werden: $f_i(\vec{r_1},...,\vec{r_N}) = 0, \quad i = 1,..., s$. Beispiel: Würfel rutscht Rampe runter oder ein Pendel.
\item nicht-holonome Zwangsbedingungen: Die Zwangsbedingungen können nicht durch Gleichungen beschrieben werden. Beispiel: Kleine Kugel rollt in einer größeren Kugel oder Teilchen in einem Kasten.
\item rheonome Zwangsbedingungen: Die Zeit $t$ tritt explizit in den Zwangsbedingungen auf.
\item skleronome Zwangsbedingungen: Die Zeit $t$ tritt nicht explizit in den Zwangsbedingungen auf.
\item differentielle Zwangsbedingungen: $ \sum_{j=1}^{3N} a_{ij}\left( \vec{r_1},...,\vec{r_N}\right)dx_j = 0 \quad i = 1,...,s$
\item konservativ: Energie ist erhalten
\item nicht-konservativ
\end{enumerate}



\textbf{Generalisierte Koordinaten:}

Anzahl Freiheitsgrade $k = 3N - s$, bei ausgedehnten Körper 3 weitere für die Rotation.
Die $3N$ Koordinaten enthalten $s$ abhängige und $k$ unabhängige Koordinaten $q_1(t), q_2(t),..., q_k(t)$, welche als generalisierte Koordinaten bezeichnet werden:

$\vec{r}_1 (t) = \begin{pmatrix}
x_1\left(q_1(t),...,q_k(t),t\right)\\y_1\left(q_1(t),...,q_k(t),t\right)\\z_1\left(q_1(t),...,q_k(t),t\right)
\end{pmatrix}
,...,
\vec{r}_N (t)= \begin{pmatrix}
x_N\left(q_1(t),...,q_k(t),t\right)\\y_N\left(q_1(t),...,q_k(t),t\right)\\z_N\left(q_1(t),...,q_k(t),t\right)
\end{pmatrix}
$

\vspace{\baselineskip}
\textbf{Größen der Mechanik in generalisierten Koordinaten:}
\begin{eqnarray*}
\text{generalisierte Geschwindigkeit}&:& \qquad \dot{q_j} = \frac{dq_j}{dt}\\
\text{generalisierte Kraft} &:& \qquad Q_m = \sum_{j=1}^N \vec{F_j}\frac{\partial\vec{r_j}}{\partial q_m}\\
\text{generalisiertes Potential} &:& \qquad V = V(q_i, \dot{q_i},t)
\end{eqnarray*}

\vspace{\baselineskip}
\textbf{Virtuelle Verrückungen:}\\
Eine \glqq virtuelle Verrückung\grqq{} (Abbildung \ref{fig:virtuelle_verrueckung}) $\delta\vec{r_j}$ des $j$-ten Massepunktes ist:
\begin{itemize}
\item infinitesimal
\item erfüllt die Zwangsbedingungen
\item zeitlos $\delta t = 0$, d.h. unendlich schnell und kann somit nicht in der Realität ausgeführt werden.
\item bei skleronomen Zwangsbedingungen quasi mit einer tatsächlichen Wegänderung im System identisch
\end{itemize}

\begin{figure}[ht]
\centering
\includegraphics[width=0.7\textwidth]{content/Bilder/virtuelle_verrueckung.png}
\caption{Beispiel für eine virtuelle Verrückung}
\label{fig:virtuelle_verrueckung}
\end{figure}

\vspace{\baselineskip}
\textbf{Zwangskräfte:}\\
Aufteilung der Kräfte $\vec{F_j}$ auf die Massen $m_j$ in äußere Kräfte $\vec{F_j}^{(a)}$ und die, durch die Zwangsbedingungen verursachte Zwangskraft $\vec{F_j}^{(z)}$\\
$\Rightarrow m_j \frac{d^2\vec{r_j(t)}}{dt^2} = \vec{F_j} = \vec{F_j}^{(a)} + \vec{F_j}^{(z)}$, \qquad $j=1, 2,..., N$\\
\begin{itemize}
\item Zwangskraft $\vec{Z}$ steht senkrecht auf dem Weg $d\vec{r}$, den die Masse $m$ zurücklegt.
\item Die Summe der virtuellen Gesamtarbeit aller Zwangskräfte verschwindet, d.h.\\ $\sum_{j=1}^N \vec{F_j}^{(z)}  \delta  \vec{r} = 0$
\end{itemize}

\textbf{Das d'alembert'sche Prinzip:}
Für sklerone bzw. rheonome Systeme gilt:

\begin{eqnarray*}
\text{Skeleronom}&:& \qquad d\vec{r_j} = \sum_{m=1}^k \frac{\partial \vec{r_j}}{\partial q_m}dq_m\\
\text{Rheonom} &:& \qquad d\vec{r_j} = \sum_{m=1}^k \frac{\partial \vec{r_j}}{\partial q_m}dq_m + \frac{\partial \vec{r_j}}{\partial t}dt
\end{eqnarray*}

$\sum_{j=1}^N \left( \vec{F_j}^{(a)} - \dot{\vec{p_j}} \right) \delta \vec{r} = 0$, \quad wobei $\dot{\vec{p_j}} = F_j$, also 2. Newton, ist.

\subsubsection{Lagrange-Funktion und Lagrange-Gleichungen}

\begin{eqnarray*}
\text{Lagrange-Funktion}&:& \qquad L = T-V, \quad \text{mit Kinetischer Energie T und Potential V} \\
\text{Lagrange-Gleichung 1. Art}&:& \qquad \frac{d}{dt}\frac{\partial T}{\partial \dot{q_i}} - \frac{\partial T}{\partial q_i} = Q_i\\
\text{Lagrange-Gleichung 2. Art} &:& \qquad \frac{d}{dt}\frac{\partial L}{\partial \dot{q_i}} - \frac{\partial L}{\partial q_i} = 0 \text{Bewegungsgleichung}
\end{eqnarray*}
 
\textbf{Bemerkungen:}
\begin{itemize}
\item Alle Bewegungsgleichungen in generalisierten Koordinanten sind durch
eine skalare Funktion $L = T - V$ bestimmt.
\item Die Wahl der generalisierten Koordinaten $q_i$ ist nicht eindeutig. Sie müssen aber unabhängig voneinander sein.
\item Die Wahl von $L$ ist ebenfalls nicht einduetig, denn $L' = L + const$. erfüllt die Lagrange-Gleichungen, wenn $L$ die Lagrange-Gleichungen erfüllt. Es ist noch weitere Flexibilität bei $L$ vorhanden ( siehe Kapitel (\ref{lagrange_symmetrie_erhaltung}))
\item Beachte die Voraussetzung: holonome Zwangsbedingungen und konservative Kräfte, d.h.: $V=V(q_i)$ nicht $V=V(\dot{q_i})$ 
\item Man kann die Newton-Gleichungen sehr einfach aus den Lagrange-Gleichungen herleiten. Für kartesische Koordianten als generalisierte Koordinaten gilt z.B. $Q_1 = F_x , Q_2 = F_y , Q_3 = F_z \Rightarrow$ 2. Newton!
\end{itemize}

\textbf{Die Bedeutung der unterschiedlichen Differentiale:}\\
\vspace{\baselineskip}
\begin{tabular}{ll}
$d \vec{r}$			& Vollständige Veränderung von $\vec{r}(t)$ im Zeitintervall $dt$ \\ 
$\delta \vec{r}$	& Virtuelle Verrückung von $\vec{r}(t)$ bei \glqq festgehaltener\grqq{} Zeit, \\
					& d.h. $\delta t = 0$, unter Beachtung der Zwangsbedingungen \\ 
$ \partial \vec{r}$ & Die Veränderung von $\vec{r}(q(t),t)$ im Zeitintervall $dt$, \\
					& die durch die explizite Zeitabhängigkeit verursacht wird. \\ 
$\vec{r}(t)$ & $ \Rightarrow \vec{r}(q_i(t),t)$ \\ 
$\vec{v}(t)$ & $= \dot{\vec{r}}(t) \Rightarrow \vec{v}(t) = \frac{d\vec{r}(q_i(t),t)}{dt} = \sum_{i=1}^{k} \frac{\partial \vec{r}}{\partial q_i} \frac{d q_i(t)}{dt} + \frac{\partial \vec{r}(q_i(t),t)}{\partial t}$ \\ 
$\vec{v}(t)$ & $\frac{d\vec{r}(q_i(t),t)}{dt} = \vec{r}(t) = \sum_{i=1}^k \frac{\partial \vec{r}}{\partial q_i}\dot{q_i} + \frac{\partial\vec{r}}{\partial t}$ \\ 
\end{tabular} 

\subsubsection{Beispiele und Anwendungen}

Falls Lagrange 1. Art : $\frac{d}{dt} \frac{\partial T}{\partial \dot{q_i}} - \frac{\partial T}{\partial q_i} = Q_i$
Dann wird $Q_i = Q_i^{(k)} + Q_i^{(R)}$ mit\\
 
$Q_i^{(k)}$ \quad Konservative Kraft, die sich aus einem (generalisierten) Potential herleiten lässt $\Rightarrow L = T-V$ \\  
$Q_i^{(R)}$ \quad Kräfte, die sich nicht aus Potentialen herleiten lassen $\Rightarrow$ bleiben stehen. \\  

$\Rightarrow \frac{d}{dt} \frac{\partial T}{\partial \dot{q_i}} - \frac{\partial T}{\partial q_i} = Q_i^{(R)}$ \quad $i=1,2,...,k$\\
Ansatz für Reibungskräfte: $Q_i^{(R)} = - \sum_{j=1}^k f_{ij}\dot{q_{j}}$\quad , mit Reibungskoeffizienten (Tensor) $f_{ij}$\\
Wenn der Reibungstensor symmetrisch ist definiert man die Funktion: $D = \frac{1}{2} \sum_{i=1}^k \sum_{j=1}^k f_{ij} \dot{q_i} \dot{q_j}$\\
D-Dissipationsfunktion $\Rightarrow Q_i^{(R)} = - \frac{\partial D}{\partial \dot{q_i}}$\\
$\Rightarrow \frac{d}{dt} \frac{\partial L}{\partial \dot{q_i}} + \frac{\partial D}{\partial \dot{q_i}} - \frac{\partial L}{\partial q_i} =0$\\

\vspace{\baselineskip}
Für weitere Beispiele siehe Skript: 141015.pdf
