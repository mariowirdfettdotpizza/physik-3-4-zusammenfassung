\subsection{Messung und ihre Ergebnisse}
\subsubsection{Die Postulate der Quantenmechanik}
\begin{enumerate}
\item[\textbf{1.}] \textbf{Postulat: Der Zustand eines Systems}\\
Der Zustand eines physikalischen Systems zu einem bestimmten Zeitpunkt $t_0$ wird durch die Angabe eines Ket-Vektors $\ket{\Psi(t_0)}$ aus dem Zustandraum \textbf{H} definiert.
\begin{itemize}
\item Der Zustandsraum ist ein Hilbert-Raum, welcher ein linearer Vektorraum ist. Somit gilt das Superpositionsprinzip.
\end{itemize}
\item[\textbf{2.}] \textbf{Postulat: Physikalische Größen}\\
Jede messbare physikalische Größe \textbf{A} wird durch einen im Zustandsraum \textbf{H} wirkenden Operator $\op{\textbf{A}}$ beschrieben. Dieser Operator ist eine Observable.
\begin{itemize}
\item Im Gegensatz zur klassischen Mechanik beschreibt die Quantenmechanik den Zustand durch einen Vektor und die physikalischen Größen durch Operatoren.
\item Eine Observable ist ein hermitescher Operator. Observablen haben somit reelle Eigenvektoren. Die Eigenvektoren bilden ein VONS.
\end{itemize}
\item[\textbf{3.}] \textbf{Postulat: Mögliche Messergebnisse}\\
Wird eine physikalische Größe \textbf{A} gemessen, dann kann das Resultat nur einer der Eigenwerte der zugehörigen Observablen $\op{\textbf{A}}$ sein.
\begin{itemize}
\item Ist das Spektrum von $\op{\textbf{A}}$ diskret, dann sind die möglichen Messwerte von $\op{\textbf{A}}$ quantisiert. Im Fall eines kontinuierlichen Spektrums tritt keine Quantisierung auf.
\end{itemize}
\item[\textbf{4.1.}] \textbf{Postulat: nichtentartetes, diskretes Spektrum}\\
Wird die physikalische Größe \textbf{A} eines Systems im normierten Zustand $\ket{\Psi}$ gemessen, dann ist die Wahrscheinlichkeit dafür, dass das Ergebnis den nichtentarteten Eigenwert $a_n$ der zugehörigen Observablen $\op{\textbf{A}}$ liefert gleich:
\begin{equation}
P(a_n)=|c_n|^2 = |\braket{u_n|\Psi}|^2
\end{equation}
\item[\textbf{4.2.}] \textbf{Postulat: entartetes, diskretes Spektrum}\\
Wird die physikalische Größe \textbf{A} eines Systems im normierten Zustand $\ket{\Psi}$ gemessen, dann ist die Wahrscheinlichkeit dafür, dass das Ergebnis den entarteten Eigenwert $a_n$ der zugehörigen Observablen $\op{\textbf{A}}$ liefert gleich:
\begin{equation}
P(a_n)=\sum_{i=1}^{g_n}|c_n^{i}|^2 = \sum_{i=1}^{g_n} |\braket{u_n^{i}|\Psi}|^2
\end{equation}
\item[\textbf{4.3.}] \textbf{Postulat: nichtentartetes, kontinuierliches Spektrum}\\
Es gilt nun:
\begin{equation}
\ket{\Psi} = \int c(\alpha) \ket{v_{\alpha}} d\alpha
\end{equation}
Wird die physikalische Größe \textbf{A} eines Systems im normierten Zustand $\ket{\Psi}$ gemessen, dann ist die Wahrscheinlichkeit dafür, dass das Ergebnis einen Wert zwischen $\alpha$ und $\alpha +d\alpha$ liefert gleich:
\begin{equation}
dP(\alpha)=|c(\alpha)|^2 d\alpha = |\braket{v_{\alpha}|\Psi}|^2 d\alpha
\end{equation}
\begin{itemize}
\item Wichtige Folgerung:\\
Für zwei Vektoren $\ket{\Psi}$ und $\ket{\Psi'}= e^{i\theta} \ket{\Psi}$ gilt:
\begin{align}
\braket{\Psi'|\Psi'}&=\braket{\Psi|e^{i\theta} e^{-i\theta}|\Psi}=\braket{\Psi|\Psi}\\
|\braket{u_n^{i}|\Psi'}|^2 &= |e^{i\theta}\braket{u_n^{i}|\Psi}|^2 = |\braket{u_n^{i}|\Psi}|^2
\end{align}
Es zeigt sich, dass zueinander proportionale Vektoren den gleichen physikalischen Zustand repräsentieren.
\item Jedoch repräsentieren die beiden Zustände $\ket{\Psi}= \lambda_1\ket{\Psi_1} + \lambda_2 \ket{\Psi_2}$ und $\ket{\varphi}= \lambda_1 e^{i\theta_1} \ket{\Psi_1} + \lambda_2 e^{i\theta_2} \ket{\Psi_2}$ nur den gleichen Zustand, wenn $\theta_1= \theta_2 + 2n\pi$
\end{itemize}
\item[\textbf{5.}] \textbf{Postulat: Reduktion des Wellenpaketes}\\
Ergibt die Messung der physikalischen Größe \textbf{A} am System im Zustand $\ket{\Psi}$ den Wert $a_n$, dann ist der Zustand $\ket{\Psi'}$ unmittelbar nach der Messung gleich $\ket{\Psi'}=\ket{u_n}$, wobei $\ket{u_n}$ ein normierter Eigenzustand des Operators $\op{\textbf{A}}$ ist. Im Fall von entarteten Eigenwerten $a_n$ ist der Zustand $\ket{\Psi'}$ unmittelbar nach der Messung gleich der auf Eins normierten Projektion
\begin{equation}
\ket{\Psi'} = \frac{\op{\textbf{P}}_n \ket{\Psi}}{\sqrt{\braket{\Psi | \op{\textbf{P}}_n | \Psi}}}
\end{equation}
von $\ket{\Psi}$ auf den zu $a_n$ gehörenden Eigenraum.
\item[\textbf{6.}] \textbf{Postulat: Zeitliche Entwicklung des Systems}\\
Die zeitliche Entwicklung des Zustandsvektors $\ket{\Psi(t)}$ wird bestimmt durch die Schrödinger Gleichung:
\begin{equation}
\im \hbar \frac{d}{dt}\ket{\Psi(t)} = \op{H}(t) \ket{\Psi}(t)
\end{equation}
Darin ist $\op{H}$ die der Gesamtenergie des Systems zugeordnete Observable (Hamilton-Operator)
von $\ket{\Psi}$ auf den zu $a_n$ gehörenden Eigenraum.
\item[\textbf{7.}] \textbf{Postulat: Korrespondenzregel}\\
Wenn der klassische Ausdruck für eine physikalische Größe $A(\vec{r}, \vec{p}, t)$ bekannt ist, dann übersetzt man ihn folgendermaßen in einen quantenmechanischen Operator $\op{A}(\op{\vec{r}}, \op{\vec{p}}, t)$:
\begin{enumerate}
\item[(i)] Man ersetze überall in dem klassischen Ausdruck $\vec{r}$ durch $\op{\vec{r}}$ und $\vec{p}$ durch $\op{\vec{p}}$.
\item[(ii)] Man beachte, dass $\op{\vec{r}}$ und $\op{\vec{p}}$ den folgenden Vertauschungsrelationen genügen: $\left[ \op{r}_j , \op{r}_k \right] = 0$, $\left[ \op{p}_j , \op{p}_k \right] = 0$, $\left[ \op{r}_j , \op{p}_k \right] = \im \hbar \delta_{jk}$
\item[(iii)] Symmetrisierungsregel: Produkte oder ähnliche Ausdrücke aus $\vec{r}$ und $\vec{p}$ müssen durch einen symmetrischen Ausdruck ersetzt werden, z.B.: $\vec{r} \cdot \vec{p} \rightarrow \frac{1}{2} \left( \op{\vec{r}} \op{\vec{p}}+ \op{\vec{p}} \op{\vec{r}} \right) $
\end{enumerate}
\end{enumerate}
%%%\subsubsection{Erwartungswert und Standardabweichung einer Observablen}
%%%Seite 27 von 150522.pdf
%%%keine neuen Inhalte
\subsubsection{kommutierende Operatoren}
Wenn zwei Operatoren $\op{A}$ und $\op{B}$ kommutieren, dann besitzen sie ein gemeinsames System von Eigenzuständen. Umgekehrt lässt sich auch sagen, dass zwei Operatoren kommutieren, wenn sie ein System gemeinsamer Eigenzustände besitzen. Diese Aussagen gelten auch für entartete Eigenzustände oder kontinuierliche Spektren.\\
Messgrößen, deren zugehörige Operatoren kommutieren, können gleichzeitig exakt gemessen werden.\\
\begin{itemize}
\item \textbf{Definition :}\\
Ein Satz von Operatoren $\op{A}_1, ..., \op{A}_n$ heißt vollständiger Satz von Observablen, wenn alle Operatoren kommutieren und das gemeinsame System von Eigenfunktionen nicht mehr entartet ist. Die Eigenfunktionen können durch die zugehörigen Eigenwerte charakterisiert werden: $\ket{\Phi_{a_1, ..., a_n}} = \ket{a_1 a_2 ... a_n}$.\\
Die Operatoren $\op{H}, \op{L}^2$ und $\op{L}_z$ bilden zum Beispiel für ein dreidimensionales, sphärisches, symmetrisches Potential einen vollständigen Satz von Operatoren.
\end{itemize}
\subsubsection{nicht kommutierende Operatoren}
Für zwei hermitesche Operatoren $\op{A}$ und $\op{B}$, die nicht kommutieren, lässt sich das Unschärfeprodukt mit Hilfe der Schwarz'schen Ungleichung
\begin{equation}
|\braket{\Psi_1 | \Psi_2}|^2 \leq \braket{\Psi_1 | \Psi_1} \braket{\Psi_2 | \Psi_2}
\end{equation}
abschätzen durch
\begin{equation}
\left( \Delta \op{A} \right)^2 \cdot \left( \Delta \op{B} \right)^2 \leq \frac{1}{4}|\braket{[\op{A}, \op{B}]}|^2 \quad \rightarrow \quad \Delta \op{A} \cdot  \Delta \op{B} \geq \frac{1}{2}|\braket{[\op{A}, \op{B}]}|
\end{equation}
Messgrößen, deren zugehörige Operatoren nicht kommutieren, können nicht gleichzeitig exakt gemessen werden, da das Resultat von der Reihenfolge der Messungen abhängt. Die zweite Messung hat z.T. die Information der ersten Messung zerstört.
\subsubsection{Wichtige Unschärfeprodukte}
Die Ort-Impuls Unschärfe
\begin{equation}
\Delta x \Delta p \leq \frac{\hbar}{2}
\end{equation}
Die Energie-Zeit Unschärfe
\begin{equation}
\Delta E \Delta t \sim \hbar
\end{equation}
Die Energie-Zeit Unschärfe besagt, dass der Energieerhaltungssatz verletzt werden kann, wenn der Zeitraum, in dem dies geschieht, nur klein genug ist. Dies ist auch beobachtbar. Ein Beispiel dafür ist die Quantenfluktuation. Kräfte werden zum Beispiel durch den Austausch von Teilchen übertragen, die kurzzeitig existieren.