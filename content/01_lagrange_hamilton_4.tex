\newcommand{\mat}[1]{\tilde{\textbf{#1}}}

\subsection{Anwendungen der Lagrange-Mechanik}

\subsubsection{Die Bewegungsgleichungen des Kreisels}
Die Betrachtung erfolgt im körperfesten System. Der Trägheitstensor $\mat{I}$ wird in ein Hauptachsensystem transformiert.
\begin{equation}
	\text{det}(\mat{I}-I\mat{E}) = \begin{vmatrix}
	I_{11}-I	& I_{12}	& I_{13} \\
	I_{12}		& I_{22}-I	& I_{23} \\
	I_{13}		& I_{23}	& I_{33}-I 
	\end{vmatrix} = 0
\end{equation}
Mit den Eigenwerten $I_1 \leq I_2 \leq I_3$ ergibt sich im Hauptachsensystem
\begin{equation}
	\mat{I}_{\text{HA}} = \mat{X}^{\textbf{T}} \mat{I} \mat{X} = \begin{pmatrix}
	I_1 & 0 & 0 \\
	0 & I_2 & 0 \\
	0 & 0 & I_3 
	\end{pmatrix}
\end{equation}
$\mat{X}$ ist hierbei die Transformationsmatrix aus den Eigenvektoren (vgl. 141031.pdf S. 2). Damit vereinfacht sich die Berechnung von $\vec{L}_{\text{HA}} = \mat{I}_{\text{HA}} \vec{\omega}_{\text{HA}}$.

Das Trägheitsmoment um eine beliebige Achse liegt immer auf einem Ellipsoid (Trägheitsellipsoid). siehe Typen symmetrischer Kreisel

\paragraph{Kreiseltypen}
Ein Kreisel ist ein starrer Körper, der sich um  einen festen Punkt
dreht. Je nach der Größe der Hauptträgheitsmomente $I_a , I_b , I_c$
gibt es verschiedene Typen von Kreiseln:
\begin{eqnarray}
I_a & \neq & I_b \neq I_c \Rightarrow \text{asymmetrischer Kreisel} \\
I_a & = & I_b \neq I_c \Rightarrow \text{symmetrischer Kreisel} \\
I_a & = & I_b = I_c \Rightarrow \text{Kugelkreisel} 
\end{eqnarray}
Die Figurenachse des symmetrischen oder des Kugelkreisels liegt dabei entlang der Hauptträgheitsachse $I_c$.

\begin{center}
\includegraphics[width=0.5\textwidth]{content/Bilder/1_4_Kreiseltypen.png}
\end{center}

Auf den \textbf{freien Kreisel} wirken keine äußeren Drehmomente oder Kräfte. Er wird im Schwerpunkt unterstützt.

Auf den \textbf{schweren Kreisel} wirken Kräfte ein, die Drehmomente bewirken.

\paragraph{Euler-Winkel}
\begin{center}
\includegraphics[width=0.5\textwidth]{content/Bilder/1_4_Eulerwinkel.png}
\end{center}

\begin{equation}
\textbf{S} \rightarrow \textbf{S'} \quad \vec{r'} =
\underbrace{\mat{D}_3 (\Psi) \mat{D}_1 (\Theta) \mat{D}_3 (\Phi)}_{\mat{D} (\Psi,\Theta,\Phi)} \vec{r}
\end{equation}
\begin{equation}
\textbf{S'} \rightarrow \textbf{S} \quad \vec{r} =
\mat{D}^{-1} (\Psi,\Theta,\Phi) \vec{r'} =
\mat{D}^{\textbf{T}} (\Psi,\Theta,\Phi) \vec{r'}
\end{equation}
\begin{multline}
\mat{D} = \begin{pmatrix}
\cos \Psi & \sin \Psi & 0 \\ -\sin \Psi & \cos \Psi & 0 \\ 0 & 0 & 1
\end{pmatrix}\begin{pmatrix}
1 & 0 & 0 \\ 0 &\cos \Theta & \sin \Theta \\ 0 & -\sin \Theta & \cos \Theta
\end{pmatrix}\begin{pmatrix}
\cos \Phi & \sin \Phi & 0 \\ -\sin \Phi & \cos \Phi & 0 \\ 0 & 0 & 1
\end{pmatrix} \\
= \begin{pmatrix}
\cos\Psi\cos\Phi-\sin\Psi\cos\Theta\sin\Phi & \cos\Psi\sin\Phi+\sin\Psi\cos\Theta\sin\Phi & \sin\Psi\sin\Theta \\
-\sin\Psi\cos\Phi-\cos\Psi\cos\Theta\sin\Phi & \cos\Psi\cos\Theta\cos\phi-\sin\Psi\sin\Phi & \cos\Psi\sin\Theta \\
\sin\Theta\sin\Phi & -\sin\Theta\cos\Phi & \cos\Theta
\end{pmatrix}
\end{multline}

\paragraph{Die Euler`schen Kreiselgleichungen}
\begin{eqnarray}
M_a = I_a \dot{\omega}_a + (I_c -I_b) \omega_b \omega_c \\
M_b = I_b \dot{\omega}_b + (I_a -I_c) \omega_a \omega_c \\
M_c = I_c \dot{\omega}_c + (I_b -I_a) \omega_a \omega_b
\end{eqnarray}
Gesucht ist die Winkelgeschwindigkeit $\vec{\omega}$ bezüglich des bewegten Hauptachsensystems.
Für den freien Kreisel ist das Drehmoment $\vec{M} = 0$.
Aus den Kreiselgleichungen folgt damit Rotationsenergieerhaltung im Hauptachsensystem. Außerdem gilt $\vert \vec{L}_{\text{HA}} \vert = \text{const.}$ Da auch im Inertialsystem \textbf{S} der Drehimpuls konstant ist, muss auch im körperfesten System der Drehimpuls konstant sein und somit $I_a \dot{\omega}_a = I_b \dot{\omega}_b = I_c \dot{\omega}_c = 0$ gelten. Die Eulergleichungen vereinfachen sich zu
\begin{equation}
(I_c -I_b)\omega_b\omega_c = (I_a -I_c)\omega_a\omega_c = (I_b -I_a)\omega_a\omega_b = 0 .
\end{equation}
Falls die Hauptträgheitsmomente paarweise verschieden
sind, dann können die Gleichungen nur erfüllt werden wenn
jeweils zwei der drei $\omega_a, \omega_b, \omega_c$ Null sind.
\begin{itemize}
\item $\vec{\omega}$ liegt in Richtung einer Hauptträgheitsachse
\item $\vec{L}_{\text{HA}}$ und $\vec{\omega}$ sind parallel
\item $\vec{\omega}$ ist in beiden Systemen konstant
\item Bei Rotationen um eine freie Rotationsachse \enquote{schlingert} ein Körper nicht.
\end{itemize}

{\Large \textbf{TODO Übungsaufgabe stabile Rotation}}

\paragraph{Die Bewegung des kräftefreien symmetrischen Kreisels}
\begin{center}
\includegraphics[width=0.5\textwidth]{content/Bilder/1_4_Kreiselachsen.png}
\end{center}
\begin{itemize}
\item $\vec{e_{z'}}\cdot(\vec{\omega}\times\vec{L}) = 0$ alle Achsen liegen in einer Ebene
\item $\frac{\tan\alpha}{\tan\beta}=\frac{I_a}{I_c}$ relative Lage der Achsen durch $I_a, I_c$ bestimmt.
\item $\vec{L}$ rotiert im Keiselsystem um die Figurenachse (\textbf{Nutation der Figurenachse} um den im Raumfesten System konstanten Vektor $\vec{L}$).
\item Nutationsfrequenz $\Omega = \frac{\vert I_a - I_c \vert}{I_a}\omega_c$ ($\omega_c = \text{const.}$ Rotationsfrequenz des Kreisels)
\end{itemize}






\subsubsection{Spezielle Kreisel}

\paragraph{Der schwere symmetrische Kreisel}
\begin{center}
\includegraphics[width=0.5\textwidth]{content/Bilder/1_4_schwerer_Kreisel.png}
\end{center}
Da die Kraft $\vec{F}$ parallel zur z-Achse ist, wirkt das Drehmoment senkrecht zu dieser. In z Richtung wirkt also kein Drehmoment und damit ist der Drehimpuls in dieser Richtung konstant. Das Drehmoment bewirkt eine Rotation von dem gesamten Drehimpuls $\vec{L}$ und damit
auch von $\vec{\omega}$ und der Figurenachse $\vec{e_{z'}}$ um die z-Achse. Das Verhalten wird als \textbf{Präzession} bezeichnet. Die Präzessionsfrequenz ist gegeben durch
\begin{equation}
\omega_P = \frac{mgl}{I\omega} \quad (\omega \gg \omega_P)
\end{equation}

Erinnerung: Die \textbf{Nutation} war die Bewegung von $\vec{\omega}$ und $\vec{e_{z'}}$ um den Drehimpuls.
\begin{center}
\includegraphics[width=0.5\textwidth]{content/Bilder/1_4_Praez_Nutation.png}
\end{center}

Mit dem Lagrangeformalismus erhält man die Erhaltungsgrößen
\begin{itemize}
\item $L_{z'} = I_c (\dot{\Phi}\cos\Theta+\dot{\Psi})=\text{const.}$
\item $L_z = I_a\dot{\Phi}\sin^2\Theta+L_{z'}\cos\Theta=\text{const.}$
\item $E = \frac{1}{2}I_a(\dot{\Theta}^2+\dot{\Phi}^2\sin^2\Theta) + \frac{1}{2}\frac{L^2_{z'}}{I_c} + mgl\cos\Theta=\text{const.}$
\end{itemize}
und die nichtlineare DGL 1. Ordnung für $\Theta(t)$
\begin{equation}
\frac{1}{2}I_a\dot{\Theta}^2+\frac{(L_z -L_{z'}\cos\Theta)^2}{2I_a\sin^2\Theta}+\frac{1}{2}\frac{L^2_{z'}}{I_c}+mgl\cos\Theta = E
\end{equation}
Nach Substitution erhält man ein elliptisches Integral. Nur qualitative Diskussion für $\Theta_2\leq\Theta(t)\leq\Theta_1$
\begin{center}
\includegraphics[width=0.5\textwidth]{content/Bilder/1_4_Praezession1.png}
\includegraphics[width=0.5\textwidth]{content/Bilder/1_4_Praezession2.png}
\end{center}

Qualitative Zusammenfassung von ein paar Beispielen. Für Rechnungen dazu siehe Skript 141103.pdf
\begin{itemize}
\item \textbf{Erde} Sonne und Mond üben auf die Erde Kräfte aus. Auf Grund der asymmetrischen Masseverteilung führt dies zu einem Drehmoment. Es ergibt sich eine Periodendauer der Präzession von 25800 Jahren(platonisches Jahr). Eine Folge davon ist, dass sich die Sternzeichen im Laufe der Zeit verschieben. Der Präzession ist eine kleine Nutation mit Periode 433 Tage (Chandler-Periode) überlagert.
\item \textbf{Stabilisierungskreisel} \\ \includegraphics[width=0.5\textwidth]{content/Bilder/1_4_Stabilisierungskreisel.png}
\item \textbf{Fahrradfahren} Beim Fahren einer Kurve wirkt die Gravitations- sowie Zentrifugalkraft, die Drehmomente auf den Radfahrer ausüben. Es muss ein Gleichgewicht der Drehmomente herrschen. Der Untergrund ist wichtig für die Betrachtung der Reibung. Eine genauere Theorie des Fahrradfahrens ist sehr komplex!!
\item \textbf{Kreiselkompass}
\item \textbf{Tippy-Top Kreisel} Die Gleitreibung übt eine horizontale Kraft auf den Kreisel aus. Dadurch ergibt sich ein vertikales Drehmoment, welches zu einer Änderung des Drehimpulses führt. Das dies so sein muss liegt daran, das der umgedrehte Kreisel höhere potentielle Energie besitzt und damit (Energieerhaltung) niedrigere Rotationsenergie. Der Drehimpuls muss sich also verringert haben.
\item \textbf{Wackelstein} Der Wackelstein hat eine asymmetrische Masseverteilung. Das körperfeste System entlang der Achsen ist kein Hauptachsensystem.
\end{itemize}






\subsubsection{Gekoppelte Oszillatoren und Übergang zum Kontinuum}

Die Lagrangefunktion für gekoppelte Oszillatoren kann durch Summation aller einzelnen Oszillatoren bestimmt werden. Aus den diskreten Oszillatoren folgt im Kontinuumslimes ein Kontinuum. Die Lagrangefunktion lässt sich dann als Integral über die Lagrangedichte ausdrücken.
Als Beispiel wird eine Saite der Länge $l$ betrachtet. Mit der Massenbelegung $\mu$ und der Spannung $F_0$ ergibt sich
\begin{equation}
L = \int_0^l \mathrm{d}x \left[\frac{1}{2}\mu\dot{y}^2-\frac{1}{2}F_0 y'^2\right] =  \int_0^l \mathrm{d}x \, \mathcal{L}
\end{equation}
Für das Hamiltonprinzip ist dann
\begin{equation}
0=\delta \int_{t_a}^{t_b}\mathrm{d}t\, L = \delta \int_{t_a}^{t_b}\mathrm{d}t \int_0^l \mathrm{d}x \, \mathcal{L}
\end{equation}
Hier müssen also zwei Variablen variiert werden. Für eine Funktion $y(x_1,x_2)$ ist die Lagrangedichte allgemein
\begin{equation}
\mathcal{L} = f(y,\frac{\partial y}{\partial x_1},\frac{\partial y}{\partial x_2},x_1,x_2)
\end{equation}

Die Euler-Lagrange-Gleichung ist dann
\begin{equation}
0 = \delta \int_{x_{1a}}^{x_{1b}}\mathrm{d}x_1 \int_{x_{2a}}^{x_{2b}}\mathrm{d}x_2\, f(\cdots) = \frac{\partial f}{\partial y}-\frac{\partial}{\partial x_1}\frac{\partial f}{\partial \left(\frac{\partial y}{\partial x_1}\right)}- \frac{\partial}{\partial x_2}\frac{\partial f}{\partial \left(\frac{\partial y}{\partial x_2}\right)}
\end{equation}

In dem Beispiel ist $x_1 \rightarrow x$ und $x_2 \rightarrow t$.
\begin{equation}
\frac{\partial\mathcal{L}}{\partial y}-\frac{\partial}{\partial x}\frac{\partial\mathcal{L}}{\partial y'}-\frac{\partial}{\partial t}\frac{\partial\mathcal{L}}{\partial \dot{y}} = 0
\end{equation}
nach dem Einsetzten erhält man die Wellengleichung. Vorsicht bei den partiellen Ableitungen. $\frac{\partial}{\partial x}$ und $\frac{\partial}{\partial t}$ entsprechen dem $\frac{\mathrm{d}}{\mathrm{d}t}$ aus der früheren Lagrange-Gleichung: $y, y', \dot{y}$ hängen von $x$ und $t$ ab und alle diese Abhängigkeiten müssen berücksichtigt werden.

Ein weiteres Beispiel für ein gekoppeltes System ist die Wellenmaschine. Sie besteht aus vielen Pendeln auf einer gemeinsamen Achse und sind durch Torsionsfedern gekoppelt. Im Kontinuumslimes erhält man eine Lagrangedichte, die mit Euler-Lagrange die Sine-Gordon-Gleichung liefert. Diese nichtlineare Wellengleichung hat Soliton-Lösungen mit bemerkenswerten Eigenschaften:
\begin{itemize}
\item genügen nicht dem Superpositionsprinzip
\item bewegen sich jedoch formstabil und verzerrungsfrei, wie Wellengruppen der ”normalen“ Wellengleichung in einem nichtdispersiven Medium
\item  können trotz Nichtlinearität unbeschädigt miteinander kollidieren.
\end{itemize}





