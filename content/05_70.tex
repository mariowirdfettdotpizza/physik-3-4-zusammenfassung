\subsection{Operatoren und Eigenfunktionen in 1D}
\subsubsection{Vektorraum $L^2(\mathds{R})$}
$L^2(\mathds{R})$ ist der Vektorraum der Quadratintegrablen Funktionen:
\begin{align}
	L^2(\mathds{R}) = \left\lbrace \Psi(x) ~\middle\vert~ \intinfty\!\mathrm{d}x \, {\vert \Psi(x) \vert}^2 < \infty \right\rbrace
\end{align}
Beliebige Linearkominationen von $\Psi_1, \Psi_2 \in L^2(\mathds{R})$ sind wieder $\in L^2(\mathbb{R})$:
\begin{align}
	\lambda_1 \Psi_1 + \lambda_2 \Psi_2 \in L^2(\mathds{R})
\end{align}
\paragraph*{Skalarprodukt}
\begin{align}
	(\varphi,\Psi) = \intinfty\!\mathrm{d}x\,\varphi^*(x) \Psi(x)
\end{align}
Regeln:
\begin{align}
	\text{Linearität im 2. Faktor:}& & (\varphi,\lambda_1 \Psi_1 + \lambda_2 \Psi_2)
	&= \lambda_1 (\varphi,\Psi_1) + \lambda_2 (\varphi,\Psi_2) \\
	\text{Antilinearität im 1. Faktor:}& & (\mu_1 \varphi_1 + \mu_2 \varphi_2,\Psi)
	&= \mu^*_1 (\varphi_1,\Psi) + \mu^*_2 (\varphi_2,\Psi)
\end{align}
\paragraph*{Orthogonalität}
\begin{align}
	(\varphi,\Psi) = 0
\end{align}
\paragraph*{($L^2$-)Norm}
\begin{align}
	(\Psi,\Psi) := \Vert \Psi \Vert^2 > 0
\end{align}
\paragraph*{Schwartz-Ungleichung}
\textit{In HöMa: Cauchy-Schwartz-Ungleichung}
\begin{align}
	\vert (\Psi_1,\Psi_2) \vert \leq \Vert \Psi_1 \Vert \cdot \Vert\Psi_2\Vert
\end{align}

\paragraph{Lineare Operatoren}
\begin{align}
	\op{A} \Psi &= \varphi\\
	\op{A} \left(\lambda_1 \Psi_1(x) + \lambda_2 \Psi_2(x) \right) &= \lambda_1\op{A}\Psi_1 + \lambda_2\op{A}\Psi_2
\end{align}
Beispiele:
\begin{align*}
	\op{x} \Psi(x) &= x \Psi(x) \\
	\op{p} \Psi(x) &= \frac{\hbar}{\im} \frac{\mathrm{d}}{\mathrm{d}x} \Psi(x) \\
	\hat{\Pi} \Psi(x) &= \Psi(-x) \quad \textit{\small (Paritätsoperator)}
\end{align*}
Beispiel für nicht-lineare Operation: \enquote{$\vert\cdot\vert^2$} \\
Produkt durch Nacheinander-Anwendung ($\Rightarrow$ nicht kommutativ)
\begin{align}
	\left(\op{A}\op{B}\right)\Psi(x) = \op{A}\left(\op{B}\Psi(x)\right) \neq \op{B}\left(\op{A}\Psi(x)\right)
\end{align}
\paragraph*{Kommutator}
Analog zu Poisson-Klammern ($\Rightarrow$ \ref{sec_Poisson})
\begin{align}
	\left[\op{A},\op{B}\right] = \op{A}\op{B} - \op{B}\op{A}
\end{align}
Falls $\left[\op{A},\op{B}\right] = 0$, so sind $\op{A}$ und $\op{B}$ kommutativ.
\paragraph*{Erwartungswert eines Operators}
für \textit{normierte} $\Psi(x)$
\begin{align}
	\langle\op{A}\rangle := \left(\Psi,\op{A}\Psi\right)
\end{align}
\paragraph*{Adjungierter Operator}
Manchmal: Berechnen von $(\varphi,\op{A} \Psi)$, mit $\varphi$ \enquote{gut}, $\Psi$ \enquote{bösartig} \\
\begin{align}
	\Rightarrow \left(\opd{A}\varphi,\Psi\right) &= \left(\varphi,\op{A}\Psi\right)
\end{align}
\paragraph*{Leicht und nützlich}
\begin{align}
	\left(\op{A},\op{B}\right)^{\dagger} &= \opd{B} \opd{A} \\
	\left[\op{A},\op{B}\right]^{\dagger} &= \left[\opd{B},\opd{A}\right] \\
	\left[\op{A}\op{B},\op{C}\right] &= \op{A} \left[\op{B},\op{C}\right] + \left[\op{A},\op{C}\right]\op{B}
\end{align}

\subsubsection{Hermitsche Operatoren}
für Hermitsche Operatoren gilt:
\begin{align}
	\opd{A} &= \op{A} \\
	\Rightarrow \langle \op{A} \rangle &= \langle \op{A} \rangle^*
\end{align}
Hermitsche Operatoren haben reelle Eigenwerte \\
$\Rightarrow$ gute Kandidaten für physikalische Messgrößen (Observablen)

\subsubsection{Eigenwerte und Eigenfunktionen}
\begin{align}
	\op{A}\Psi(x) &= a\Psi(x)
\end{align}
\begin{center}$a$ := Eigenwert von $\op{A}$,\qquad $\Psi(x)$: Eigenfunktion von $\op{A}$\end{center}

\subsubsection*{Entartung}
Wenn $a$ gleicher Eigenwert von zwei linear unabhängigen Funktionen $\Psi_1, \Psi_2$ ist:
\begin{itemize}
	\item $a$ ist (zweifach) entartet.
	\item jede Linearkombination von $\Psi_1, \Psi_2$ ist wieder Eigenfunktion von a (\enquote{entarteter Unterraum})
\end{itemize}
\subsubsection*{Erwartungswert im Eigenzustand}
\begin{align*}
	\langle \op{A} \rangle &= a & \langle \op{A}^2 \rangle &= a^2
\end{align*}
in Eigenzustand: Erwartungswert = Eigenwert
\begin{align}
	\Delta \op{A}^2 = 0
\end{align}
$\rightarrow \op{A}$ hat keine Unschärfe, sondern ergibt exakt $a$.
\subsubsection*{Orthogonalität von Eigenfunktionen}
Eigenfunktionen zu \textit{verschiedenen} Eigenwerten sind orthogonal.
\begin{align*}
	\op{A}\Psi &= a \Psi & \op{A}\varphi &= a'\varphi
\end{align*}
\begin{align*}
	\Rightarrow \left(\Psi,\varphi\right) = 0
\end{align*}

\subsubsection*{Orthogonalisieren bei Entartung}
Eigenfunktionen $\Psi_1,\Psi_2$ zum gleichen Eigenwert sind nicht orthogonal.\\
Orthogonalisieren (noch nicht normiert):
\begin{align*}
	\tilde{\Psi}_2 = \Psi_2 - \left(\Psi_2,\Psi_1\right)\Psi_1
\end{align*}
Normierung:
\begin{align}
\tilde{\tilde{\Psi}}_2 = \frac{\tilde{\Psi}_2}{\sqrt{\left(\tilde{\Psi}_2,\tilde{\Psi}_2\right)}}
\end{align}
Normierte Eigenfunktionen eines Hermitschen Operators bilden ein orthonormales System.
\subsubsection*{Vollständiges Orthonormalsystem (VONS)}
$\rightarrow$ Wenn jedes $\Psi(x)$ durch Superposition der Eigenfunktionen dargestellt werden kann, so heißt das ONS \enquote{vollständig} (VONS)
\begin{align*}
	\text{orthogonale Eigenfunktionen}&\quad\varphi_n(x)\\
	\Rightarrow \Psi(x) = \sum\limits_n c_n \varphi_n(x)&
\end{align*}
Bestimmung der Entwicklungskoeffizienten
\begin{align}
	c_n = \left(\varphi_n,\Psi\right) = \intinfty\!\mathrm{d}x\,\varphi_n^*(x) \Psi(x)
\end{align}
\subsubsection*{Vollständigkeitsrelation}
\begin{align}
	\sum\limits_n \varphi_n^*(y) \varphi_n(x) = \delta(y-x)
\end{align}
\subsubsection{Impulseigenfunktion}
$p$ sei Eigenwert von $\op{p}$. ($p \in \mathds{R}$)
\begin{align}
	\Psi_p(x) &= \frac{1}{\sqrt{2\pi\hbar}}e^{\im\frac{px}{\hbar}} \qquad \left(\not\in L^2(\mathds{R})\right)\\
	\op{p} \Psi_p(x) &= \frac{\hbar}{\im}\frac{\mathrm{d}}{\mathrm{d}x}\Psi_p(x) = p \Psi_p(x)
\end{align}
Orthogonalität:
\begin{align}
	\left(\Psi_{p'},\Psi_p \right) = \delta(p-p')
\end{align}
\enquote{Dirac-Normierung} immer bei Eigenfunktionen zu kontinuierlichen Eigenwerten.
\begin{align}
	\intinfty\!\mathrm{d}p\,\Psi_p^*(y)\Psi_p(x) = \delta(y-x)
\end{align}
\subsubsection{Ortseigenfunktion}
Gesucht: $\Psi_{x_0}(x)$ \quad ($x_0$: gewünschter Orts-Eigenwert)
\begin{align}
	\op{x}\Psi_{x_0}(x) = x \Psi_{x_0}(x) \overset{!}{=} x_0 \Psi_{x_0}(x)\\
	\Rightarrow \Psi_{x_0}(x) = \delta(x-x_0)
\end{align}
Orthogonalität:
\begin{align}
	\left(\Psi_{x_0},\Psi_{x_1}\right) = \delta(x_0-x_1)
\end{align}
