DOC       = Zusammenfassung_3_4
PDFLATEX  = pdflatex -output-directory=build -synctex=1 -interaction=nonstopmode -halt-on-error $(DOC).tex
VIEWPDF   = start build/$(DOC).pdf

all: build/$(DOC).pdf
	
build/$(DOC).pdf: $(DOC).tex content/* content/Bilder/* | build
	$(PDFLATEX)
	$(PDFLATEX)
	$(VIEWPDF) 
	
pdf: $(DOC).tex content/* content/Bilder/* | build
	$(PDFLATEX)
	$(VIEWPDF)
	
fast: $(DOC).tex content/* content/Bilder/* | build
	$(PDFLATEX)
	$(VIEWPDF)
	
build:
	mkdir -p build

clean:
	rm -rf build