# Aufteilung der Physik Zusammenfassung #

## Physik III ##
### Kapitel 1: Lagrange-Hamilton-Formalismus ###
Kapitel		| Bearbeiter	| Status
------------|---------------|--------------
1.1			| Felix P.		| **FERTIG**
1.2			| Nico			| **FERTIG**
1.3			| Nico			| **FERTIG**
1.4			| Christian		| **FERTIG**
1.5			| Jasmin		| **FERTIG**
1.6			| Jasmin		|

### Kapitel 2: Wellen ###
Kapitel		| Bearbeiter	| Status
------------|---------------|--------------
2.1			| Vanessa		| **FERTIG**
2.2			| Vanessa		| **FERTIG**
2.3			| Vanessa		| **FERTIG**
2.4			| Mario			| **FERTIG**
2.5			| *wer grad Zeit hat..*		| 

### Kapitel 3: Optik ###
Kapitel		| Bearbeiter	| Status
------------|---------------|--------------
3.1			| Kalla			| **FERTIG**
3.2			| Kalla			| **FERTIG**
3.3			| Felix M.		| 

### Kapitel 4: Ausgleichsvorgänge ###
Kapitel		| Bearbeiter	| Status
------------|---------------|--------------
4.1			| Jasmin		| **FERTIG**
4.2			| Jasmin		| 

## Nach Namen ##
* Christian:	1.4
* Felix M.:		3.3
* Felix P.:		1.1
* Jasmin:		1.5 1.6 4.1 4.2
* Kalla:		3.1 3.2
* Mario:		2.4
* Nico:			1.2 1.3
* Vanessa:		2.1 2.2 2.3


## Physik IV ##
### Kapitel 5: Quantenmechanik: Grundlagen ###

Kapitel																					| Bearbeiter	| Status
----------------------------------------------------------------------------------------|---------------|--------------
64 Mikroskopische Grenzen der Klassischen Physik: Der Aufbau der Materie				|	Nico		| **FERTIG**
65 Materiewellen, Doppelspaltversuch & Wahrscheinlichkeiten								|	Nico		| **FERTIG**
66 Die Schrödingergleichung																|	Jasmin		| **FERTIG**
67 Materiewellen und ihre Dynamik														|	Mario		| **FERTIG**
68 Mittelwerte und Schwankungen															|	Mario		| **FERTIG**
69 Orts- und Impulsdarstellung															|	Kalla (*getauscht*)		| **FERTIG**
70 Operatoren und Eigenfunktionen														|	Kalla (*getauscht*)		| **FERTIG**
71 Schrödingergleichung für stückweise konstante Potentiale I: Allgemeine Betrachtungen	|	Vanessa					| **FERTIG**
72 Schrödingergleichung für stückweise konstante Potentiale II: Potentialtöpfe			|	Christian (*getauscht*)	| **FERTIG**
73 Schrödingergleichung für stückweise konstante Potentiale III: Reflexion und Transmission	|	Felix M.	|
74 Dirac-Schreibweise																	|	Felix P.	| **FERTIG**
75 Der harmonische Oszillator															|	Nico		| **FERTIG**
76 Lösungstechnikern für die zeitabhängige Schrödingergleichung							|	Jasmin		| **FERTIG**
77 Weitere allgemeine Eigenschaften der Schrödingergleichung							|	Mario		| **FERTIG**
78 Die quasiklassische (WKB) Näherung													|	Christian	| **FERTIG**
79 Tunneleffekt und radioaktiver Zerfall												|	Vanessa		| **FERTIG**
80 Mathematische Ergänzungen zu Operatoren und Hilberträumen							|	Kalla		| **FERTIG**
81 Symmetrien und Erhaltungssätze														|	Felix M.	|
82 Zeitentwicklung, Schrödingerbild und Heisenbergbild									|	Felix P.	| **FERTIG**
83 Messungen und ihre Ergebnisse														|	Nico		| **FERTIG**

### Kapitel 6: Das Wasserstoff-Atom ###
Kapitel																					| Bearbeiter	| Status
----------------------------------------------------------------------------------------|---------------|--------------
84 Ein Teilchen im Zentralpotential I													|	Jasmin		| **FERTIG**
85 Eigenwerte des Drehimpulsoperators													|	Mario		| **FERTIG**
86 Ein Teilchen im Zentralpotential II													|	Christian	| **FERTIG**
87 Das Wasserstoffatom I																|	Vanessa		| **FERTIG**
88 Das Wasserstoffatom II																|	Kalla		| **FERTIG**
89 Wasserstoffähnliche Systeme															|	Felix M.	|
90 Geladenes Teilchen im elektromagnetischen Feld										|	Felix P.	| **FERTIG**
91 Elektron im zeitlich konstanten Magnetfeld											|	Nico		| **FERTIG**
92 Der Spin: Motivation																	|	Jasmin		| **FERTIG**
93 Der Spin: Formalismus																|	Mario		| **FERTIG**
94 Addition von Drehimpulsen															|	Christian	| **FERTIG**
95 Stationäre Störungsrechnung ohne Entartung											|	Vanessa		| **FERTIG**
96 Stationäre Störungsrechnung mit Entartung											|	Kalla		| **FERTIG**
97 Das Rayleigh-Ritz Variationsprinzip													|	Felix M.	|
98 Zeitabhängige Störungstheorie: Fermis goldene Regel									|	Felix P.	|
99 Optische Übergänge & Laser															|	Nico		| **FERTIG**

### Kapitel 7: Atom- und Molekülphysik ###
Kapitel																					| Bearbeiter	| Status
----------------------------------------------------------------------------------------|---------------|--------------
100 Identische Teilchen: Grundlagen														|	Jasmin		| **FERTIG**
101 Identische Teilchen: Anwendungen													|	Mario		| **FERTIG**
102 Spektren wasserstoffähnlicher Systeme												|	Christian	|
103 Das Heliumatom																		|	Vanessa		| **FERTIG**
104 Atomaufbau - Hund'sche Regeln														|	Kalla		|
105 Das Wasserstoff-Molekül																|	Felix M.	|
106 Die chemische Bindung																|	Felix P.	|
107 Letzte Vorlesung...																	|	Nico		| nicht mehr wirklich relevant